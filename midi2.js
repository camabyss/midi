
function getKey (ev) {
	// Gets keyCode on all browsers
	var k;
	if ('key' in ev && ev.key && ev.key.length == 1)
		k = ev.key.charCodeAt(0);
	else if (window.event)
		k = ev.keyCode;
	else k = ev.which;
	return k;
}

function Interface($parent) {
	// Main application interface

	if ($parent === undefined)
		$parent = $('body');

	// Save "this" scope somewhere permanent (for maintaingin scope in callbacks)
	var _this = this,
		$bar = $('<header>',{id:'bar'});
	// Begin building interface elements, most of this work is now done by each interface class individually
	$parent
		.append($bar);

	// JS has no built-in initialisation function, as you can just run code as above ^^^
	// But it's best to group it into a function then call it at the end
	this.init = function () {

		// Creating instrument interface, instrument model has already been defined globally
		this.instruments = new InstrumentInterface(instruments);

		// Update the current track's intrument when it is changed
		this.instruments.callbacks({
			change:function (instrument) {
				_this.tracks.currentTrack().model
						.set('instrument', instrument);
				instruments.working(_this.tracks);
			}
		});

		// Display the initial instruments
		this.instruments.display();


		// New storage interface and model for localStorage
		this.storage = new StorageInterface(new Storage(), $bar);

		// Display results when actions are taken
		this.storage.callbacks({
			load: function (name, file) {
				var self = this;
				_this.popup();
				_this.popup('Loading Instruments...', 'loading');

				// Go through the tracks, looking for unloaded instruments, loading them one by one then calling this functiona again when done
				function loady(instrument) {
					if (! instruments.isLoaded(instrument)) {
						instruments.loadInstrument(instrument, function () {self.load(name,file)});
						return false;
					} else return true;
				}
				for (var F in file) {
					var instrument = file[F].instrument_name;
					// if (! instrument)
					// 	file[F][0] = instrument = instruments[0];
					//

					if (typeof instrument == 'string') {
						if (!loady(instrument)) return false;
						var ino = ino = instruments.get(instrument);
						if (file[F].instrument.indexOf(ino) == -1)
							file[F].instrument.push(ino);
					} else {
						for (var i in instrument) {
							var ins = instrument[i]
							if (!loady(ins)) return false;
							var ino = ino = instruments.get(ins);
							if (file[F].instrument.indexOf(ino) == -1)
								file[F].instrument.push(ino);
						}
					}
				}
				// Stop anything still playing
				_this.tracks.stop();
				// Close the popup
				_this.popup();
				// Set the name input and load the tracks
				_this.storage.name(name);
				_this.tracks.reBuild(file);
				instruments.working(_this.tracks)
			},
			save: function () {
				// Notify user of save
				_this.pulse('save');
			},
			new: function () {
				// Stop anything still playing
				_this.tracks.stop();
				// Clear name and build empty tracks
				_this.storage.name('');
				_this.tracks.reBuild([]);
				instruments.working(_this.tracks)
				// Notify the user of creation
				_this.pulse('plus');
			},
			getData: function () {
				// Get current tracks to save
				return _this.tracks.getAll();
			}
		});

		// New notes interface (keyboard), notes model is already defined gloablly
		this.notes = new NoteInterface(notes);

		// Carry the note through to the tracks
		// ERR: perhaps the notes interface should play the note... Callback crossover should be on the track side
		this.notes.callbacks({
			noteOn:function (note) {
				_this.tracks.noteOn(note);
			},
			noteOff:function (note) {
				_this.tracks.noteOff(note);
			}
		})

		$parent.append(this.notes.view);

		// New tracks interface, this is a combination interface and collection model (list of tracks)
		this.tracks = new Tracks($bar, $parent);

		this.tracks.callbacks({
			active:function (track) {
				var instrument = track.get('instrument');
				_this.instruments.selectInstrument(instrument);
				// track.setProgram(instrument);
				_this.notes.deactivateAll();
			},
			noteOff:function (note, track, delay) {
				var fscope = this;
				if (delay) {
					return window.setTimeout(function () {
						fscope.noteOff(note, track, 0);
					}, delay);
				}
				if (_this.tracks.currentTrack(track))
					_this.notes.deactivate(note);
			},
			noteOn:function (note, track, delay) {
				var fscope = this;
				if (delay) {
					return window.setTimeout(function () {
						fscope.noteOn(note, track, 0);
					}, delay);
				}
				if (_this.tracks.currentTrack(track))
					_this.notes.activate(note);
			}
		});
		// Select the default track
		this.tracks.activeTrack();

		$bar.append(this.instruments.view);
		// ERR ask to auto load?
		this.storage.autoLoad();

	}

	this.popup = function (message, type) {
		// Create a popup (message[, type])
		// or destroy a popup (*no args*)
		if (message === undefined)
			message = false;
		if (message == false)
			return $('#popup').remove();
		var $popup = $('#popup');
		if (! $popup.length) {
			$popup = $('<div>',{id:'popup'});
			$parent.append($popup);
			$popup.dblclick(function() {$(this).remove();});
		}
		$popup.html(message);
		if (type !== undefined)
			$popup.addClass(type);
	}
	this.pulse = function(icon) {
		// Momentarily pulse a font-awesome icon in the center of the screen
		var $icon = $('<i>',{class:'pulse fa fa-'+icon});
		$icon.css({opacity:0,fontSize:100})
		$parent.append($icon);
		$icon.animate({opacity:1,fontSize:200},100,function () {
			$icon.animate({opacity:0,fontSize:150},100,function () {
				$icon.remove();
			})
		})
	}
	$(document).keydown(function (ev) {
		var key = getKey(ev);
		if (key > 90) key -= 32;
		// Ctrl + S
		if (key == 27)
			_this.popup();
		if (ev.ctrlKey && key == 83) {
			_this.storage.save();
			ev.preventDefault();
			ev.stopPropagation();
			return false;
		}
		// Ctrl + O
		if (ev.ctrlKey && key == 79) {
			_this.storage.load();
			ev.preventDefault();
			ev.stopPropagation();
			return false;
		}
		// Ctrl + N
		/* doesn't override
		if (ev.ctrlKey && key == 78) {
			_this.storage.new();
			ev.preventDefault();
			ev.stopPropagation();
			return false;
		} */
	})


	// Calling the initialisation function after everything else has been defined
	this.init();
}



function Notes(layout, country, shiftFlat, ostart) {
	// A map from a keyboard layout to a note system
	// Only Qwerty and Dvorak supported, correct number-row display depends on uk or us layouts

	// Default to qwerty:uk
	if (layout === undefined)
		layout = 'qwerty';
	if (country === undefined)
		country = 'uk';
	if (shiftFlat === undefined)
		shiftFlat = true;
	if (ostart === undefined)
		ostart = 4;
	if (ostart < 1) ostart = 1;
	if (ostart > 8) ostart = 8;
	var cstart = "C"+ostart;
	// Define possible layouts
	var layouts = {
			qwerty:"1234567890qwertyuiopasdfghjklzxcvbnm",
			dvorak:"1234567890pyfgcrlaoeuidhtnsqjkxbmwvz"
		},
		symbols = {
			uk:{
				")":0,
				"!":1,
				'"':2,
				"\u00A3":3,
				"$":4,
				"%":5,
				"^":6,
				"&":7,
				"*":8,
				"(":9,
				'¬':'`',
				'_':'-',
				'+':'=',
				'{':'[',
				'}':']',
				':':';',
				'@':"'",
				'~':'#',
				'|':'\\',
				'<':',',
				'>':'.',
				'?':'/'
			},
			// uk:[")","!",'"',"\u00A3","$","%","^","&","*","("],
			us:{
				")":0,
				"!":1,
				"@":2,
				"#":3,
				"$":4,
				"%":5,
				"^":6,
				"&":7,
				"*":8,
				"(":9
			}
		},
	// Note/key/char maps
		charToNote = {},
		noteToChar = {},
		keyToNote = {},
		noteToKey = {};
	this.flatLayout = function (sf) {
		if (sf === undefined)
			return shiftFlat;
		else shiftFlat = sf;
		this.setLayout(layout);
	}
	this.setLayout = function (lay) {
		// Set layout and re-build notes maps
		if (lay in layouts)
			layout = lay;
		else {
			layout = 'custom';
			layouts.custom = lay;
		}
		charToNote = this.notesFromLayout();
		noteToChar = {};
		keyToNote = {};
		noteToKey = {};
		for (var k in charToNote) {
			var K = k*1,
				note = charToNote[K],
				bnote = note[0] + 'b' + note[1];
			noteToChar[note] = K;
			noteToKey[note] = K;
			keyToNote[K] = note;

			if (shiftFlat && bnote in MIDI.keyToNote) {
				var bk = (k*1)-0.5;
				noteToKey[bnote] = bk;
				keyToNote[bk] = bnote;
			}
		}
	}
	this.notesFromLayout = function () {
		// Build comprehensive keyToNote map from keyboard layout
		// Finds the middle key of your keyboard and calls it middle C, then it works its way out defining the keys above and below that in the form Ab4 (Generally: [A-G]b?[0-9])
		var ll = layouts[layout].length,
			keyToNote = {},
			midc = Math.round(ll/2),
			mc = MIDI.keyToNote[cstart];
		if (shiftFlat) {
			var more = 0,
				less = 0;
			keyToNote[midc] = cstart;
			var C = 67,
				UC = 67,
				DC = 67,
				UOC = ostart,
				DOC = ostart,
				LS = 65,
				LB = 71;
			for (var i=0;i<midc;i++) {
				UC += 1;
				DC -= 1;
				if (UC>LB)
					UC = LS;
				if (UC == C)
					UOC += 1;

				if (DC<LS)
					DC = LB;
				if (DC == C-1)
					DOC -= 1;

				if (midc+1+i < ll) {
					var str = String.fromCharCode(UC)+UOC;
					if (str in MIDI.keyToNote)
						keyToNote[midc+1+i] = str;
					else less += 1;
				}
				if (midc-(1+i) >= 0) {
					var str = String.fromCharCode(DC)+DOC;
					if (str in MIDI.keyToNote)
						keyToNote[midc-(1+i)] = str;
					else more += 1;
				}
			}
			// Shuffle up if A0 limit reached
			if (more) {
				var nk = {};
				for (var k in keyToNote)
					nk[k-more] = keyToNote[k];
				for (var m=0;m<more;m++) {
					nk[(ll-more)+m] = String.fromCharCode(UC)+UOC;
					UC += 1;
					if (UC>LB)
						UC = LS;
					if (UC == C)
							UOC += 1;
				}
				keyToNote = nk;
			}
			if (less) {
				var nk = {};
				for (var k in keyToNote)
					nk[(k*1)+less] = keyToNote[k];
				for (var m=0;m<less;m++) {
					DC -= 1;

					if (DC<LS)
						DC = LB;
					if (DC == C-1)
						DOC -= 1;
					nk[less-(m+1)] = String.fromCharCode(DC)+DOC;
				}
				keyToNote = nk;
				return keyToNote;
			}
		}
		else {
			var mc = MIDI.keyToNote[cstart],
				a0 = MIDI.keyToNote['A0'],
				c8 = MIDI.keyToNote['C8'];
			if (mc-midc<a0)
				mc = a0+midc;
			if (mc+midc>c8)
				mc = c8-(midc-1);
			for (var i=0;i<ll;i++)
				keyToNote[i] = MIDI.noteToKey[(mc-midc)+i];
		}
		return keyToNote;
	}
	this.octave = function (oct) {
		if (oct === undefined)
			return ostart;
		else ostart = oct;
		if (ostart < 1) ostart = 1;
		if (ostart > 8) ostart = 8;
		cstart = "C"+ostart;
		this.setLayout(layout);
	}

	this.charCount = function () {
		// Returns the length of the current layout
		return layouts[layout].length;
	}
	this.midiCount = function () {
		var a = MIDI.keyToNote['A0'],
			z = MIDI.keyToNote['C8'];
		var ret = (z-a)+1;
		// if (shiftFlat) {
		// 	for (var i=a;i<=z;i++) {
		// 		if (MIDI.noteToKey[i][1] == 'b')
		// 			ret -= 1;
		// 	}
		// }
		return ret;
	}
	this.midiToNote = function (n) {
		var a = MIDI.keyToNote['A0'],
			k = n+a;
		// if (shiftFlat) k = Math.round(k*2);
		return MIDI.noteToKey[k];
	}
	this.noteToMidi = function (note) {
		var a = MIDI.keyToNote['A0'],
			ret = MIDI.keyToNote[note]-a;
		// if (shiftFlat) ret /= 2;
		return ret;
	}

	this.keyToNote = function (key) {
		if (key in keyToNote)
			return keyToNote[key];
		return false;
	}
	this.noteToKey = function (note) {
		// Acceses noteToKey map
		if (note in noteToKey)
			return noteToKey[note];
		return false;
	}
	this.noteToLetter = function (note) {
		// Finds the letter in the current layout that corresponds to the given note
		var bkey = noteToKey[note],
			key = Math.ceil(bkey),
			letter = layouts[layout][key];
		if (key == bkey)
			return letter;
		if (letter.match(/[a-z]+/))
			return letter.toUpperCase();
		else {
			for (var s in symbols[country]) {
				if (symbols[country][s] == letter)
					return s;
			}
		}
	}
	this.charCodeToNote = function (k, flat) {
		// Finds the note from a keyboard event charCode
		// A - Z
		if (k>64&&k<91)
			k += 32;

		var str = String.fromCharCode(k);
		if (str in symbols[country] && flat)
			str = symbols[country][str];

		var noteIndex = layouts[layout].indexOf(str);
		if (noteIndex == -1)
			return 'falsy';

		var note = keyToNote[noteIndex];
		if (shiftFlat && flat)
			note = note[0] + "b" + note[1];
		return note;
	}
	this.each = function (type, callback) {
		// Lets you loop through the maps
		// ERR: Not all maps implemented, only those used
		if (type == 'char') {
			for (var c in charToNote) {
				var C = c*1;
				callback(C,charToNote[C],layouts[layout][C]);
			}
		} else
		if (type == 'key') {
			for (var c in keyToNote) {
				var C = c*1;
				callback(C,keyToNote[C]);
			}
		}
		if (type == 'midi') {
			var a = MIDI.keyToNote['A0'],
				z = MIDI.keyToNote['C8'];
			for (var i=a;i<=z;i++) {
				// if ((!shiftFlat) || (MIDI.noteToKey[i][1] != 'b'))
					callback(i-a,MIDI.noteToKey[i]);
			}
		}
	}
	this.falseNotes = function () {
		// Return a dictionary of notes, each with the value false
		// This is used to keep track of which keys are down
		var ret = {};
		for (var c in keyToNote)
			ret[keyToNote[c]] = false;
		return ret;
	}
	this.noteDistance = function (a, b) {
		return MIDI.keyToNote[b]-MIDI.keyToNote[a];
	}
	this.distanceNote = function (note, distance) {
		return MIDI.noteToKey[MIDI.keyToNote[note]+distance];
	}

	// Set & build initial layout
	this.setLayout(layout);
}

function NoteInterface(notes, $parent) {
	var _this = this,
		hidden = false,
		$notes = $('<ol>',{id:'notes'}),
			$toggle = $('<div>',{id:'notes-toggle',class:'fa fa-keyboard-o'}).html(' Keyboard'),
				$flatSwitchLabel = $('<label>',{}),
					$flatSwitch = $('<input>',{type:'checkbox'}),
				$octNumLabel = $('<label>',{}),
					$octNum = $('<input>',{type:'number',min:1,max:8}),
		notesPressed = {},
		callbacks = {
			noteOn:false,
			noteOff:false
		};
	this.model = notes;
	this.callbacks = function (cbs) {
		for (var c in cbs)
			callbacks[c] = cbs[c];
	}

	$notes.append($toggle
		.append($flatSwitchLabel
			.append($flatSwitch)
			.append(" Flat Notes"))
		.append($octNumLabel
			.append($octNum)
			.append(" Octave")));
	function noteOn (note){
		notesPressed[note] = true;
		if (callbacks.noteOn)
			callbacks.noteOn(note);
	}
	function noteOff (note) {
		notesPressed[note] = false;
		if (callbacks.noteOff)
			callbacks.noteOff(note);

	}
	function noteSwitch (ev, onOff) {
		if (ev.ctrlKey) return;
		if ($(ev.target).is('input,select'))
			return;
		var key = getKey(ev),
			note = notes.charCodeToNote(key, ev.shiftKey);
		if (note == 'falsy' && key == 16) {
			for (var n in notesPressed) {
				if ((n[1] != 'b') == onOff &&
					notesPressed[n]) {
					if (onOff) {
						noteOff(n);
					} else {
						note = n;
						break;
					}
				}
			}
		}
		if (note != 'falsy') {
			if (onOff) {
				if (! notesPressed[note])
					noteOn(note);
			} else noteOff(note);
		}

	}

	this.deactivateAll = function () {
		$notes.find('.active').removeClass('active');
	}
	this.deactivate = function (note) {
		if (hidden) return;
		$notes.find('.note[data-note='+note+']').removeClass('active');
	}
	this.activate = function (note) {
		if (hidden) return;
		$notes.find('.note[data-note='+note+']').addClass('active');
	}
	function buildNotes () {
		$notes.find('.note').remove();
		notes.each('char', function (char, note, letter) {

			var bnote = note[0] + 'b' + note[1],
				$note = $('<li>',{class:'note','data-note':note,'data-key':letter});
			notesPressed[note] = false;
			$notes.append($note);
			$note.css({width:(100/notes.charCount())+"%"})
			if (! notes.flatLayout() && note[1] == 'b')
				$note.addClass('flat');

			if (notes.flatLayout() && bnote in MIDI.keyToNote) {
				notesPressed[bnote] = false;
				$note.append($('<div>',{class:'note flat','data-note':bnote,'data-key':notes.noteToLetter(bnote)}));
			}
		});
	}
	buildNotes();

	$('body').keydown(function (ev) {
		noteSwitch(ev, true);
	});
	$('body').keyup(function (ev) {
		noteSwitch(ev, false);
	});

	$flatSwitch.prop('checked',notes.flatLayout()).change(function () {
		notes.flatLayout($(this).prop('checked'))
		buildNotes();
	})
	$octNum.val(notes.octave()).change(function () {
		notes.octave($(this).val()*1)
		buildNotes();
	})
	$toggle.click(function (ev) {
		if (ev.target != this) return;
		$notes.toggleClass('hidden');
		_this.deactivateAll();
		hidden = $notes.is('.hidden');
		$(window).resize();
	})

	this.view = $notes;

	if ($parent !== undefined)
		$parent.append($notes);

}



function Storage() {
	var prefix = 'midi-track-',
		pl = prefix.length;
	this.save = function (name, tracks, pre) {
		if (pre == undefined) pre = prefix;
		var store = [];
		for (var t in tracks) {
			var track = tracks[t].model;
			if (track.get('notes').length) {
				var inst = track.get('instrument'),
					instrums = [];
				if (inst instanceof Array) {
					for (var i in inst)
						instrums.push(instruments.get(inst[i]));
				} else instrums = instruments.get(inst);

				store.push([
					instrums, track.get('notes'), track.get('start'),track.get('stop'),track.get('offset'),track.get('loop'),track.get('muted'),
					track.get('tempo'),track.get('bar'),track.get('sequence'),
					track.get('volume')]);
			}
		}
		if (store.length) {
			localStorage[pre+name] = JSON.stringify(store);
			return true;
		}
		return false;
	}
	this.load = function (name,pre) {
		if (pre === undefined) pre = prefix;
		var fullName = pre+name;
		if (! (fullName in localStorage))
			return false;
		var file = JSON.parse(localStorage[fullName]),
			ret = [];

		for (var F in file) {
			var f = file[F],
				track = {};
			track.notes = f[1];
			track.instrument_name = f[0];
			track.instrument = [];
			// Cascading optional vars are for my benefit, so that older versions will still load, you can expect all your saves to have the same data and not need these ifs
			if (f.length > 2) {
				track.start = f[2];
				track.stop = f[3];
				track.offset = f[4];
				track.loop = f[5];
				track.muted = f[6];
				if (f.length > 7) {
					track.tempo = f[7];
					track.bar = f[8];
					if (f.length > 9) {
						track.sequence = f[9];
						if (f.length > 10)
							track.volume = f[10];
					}
				}
			}
			ret.push(track);
		}
		return ret;
	}
	this.list = function () {
		var ret = [];
		for (var l in localStorage) {
			if (l.slice(0,pl) ==  prefix)
				ret.push(l.slice(pl));
		}
		return ret;
	}
	this.autoSave = function (name,tracks) {
		localStorage['midi-auto-save-name'] = name;
		this.save('',tracks,'midi-auto-save');
		console.log('auto save')
	}
	this.autoLoad = function () {
		return {file:this.load('','midi-auto-save'),name:localStorage['midi-auto-save-name']};
	}
	this.saveCats = function (type,cats) {
		localStorage['midi-categories-'+type] = JSON.stringify(cats);
	}
	this.loadCats = function (type) {
		var pre = 'midi-categories-';
		if (type === undefined) {
			var ret = {};
			for (var d in defaultCats)
				ret[d] = this.getCats(d);
			return ret;
		} else {
			var name = pre+type;
			if (name in localStorage)
				return JSON.parse(localStorage[name]);
			else return defaultCats[type];
		}
	}
	var defaultCats = {
		track:{},
		instrument:{
			"drum":["steel_drums","taiko_drum","synth_drum","reverse_cymbal"],
			"wind":["clarinet","oboe","bassoon","blown_bottle","breath_noise","english_horn","flute","ocarina","pan_flute","piccolo","recorder","shakuhachi","shanai"],
			"string":["viola","violin","banjo","fiddle","orchestral_harp","pizzicato_strings","string_ensemble_1","string_ensemble_2","tremolo_strings","cello","contrabass","koto","shamisen"],
			"brass":["alto_sax","tenor_sax","soprano_sax","baritone_sax","muted_trumpet","trumpet","trombone","french_horn","brass_section","tuba"],
			"percussive":["woodblock","glockenspiel","agogo","celesta","kalimba","marimba","melodic_tom","timpani","tinkle_bell","tubular_bells","vibraphone","xylophone"],
			"guitar":["acoustic_guitar_nylon","acoustic_guitar_steel","overdriven_guitar","guitar_harmonics","guitar_fret_noise","electric_guitar_muted","electric_guitar_jazz","electric_guitar_clean","distortion_guitar","banjo","fretless_bass","electric_bass_pick","electric_bass_finger","acoustic_bass","slap_bass_1","slap_bass_2"],
			"effects":["voice_oohs","gunshot","helicopter","telephone_ring","applause","bird_tweet","blown_bottle","breath_noise","choir_aahs","fx_1_rain","fx_2_soundtrack","fx_8_scifi","fx_7_echoes","fx_6_goblins","fx_5_brightness","fx_4_atmosphere","fx_3_crystal","orchestra_hit","reverse_cymbal","seashore","sitar","whistle"],
			"synth":["pad_3_polysynth","synth_drum","synth_bass_2","synth_bass_1","lead_2_sawtooth","lead_1_square","pad_1_new_age","pad_8_sweep","pad_7_halo","pad_6_metallic","pad_5_bowed","pad_4_choir","pad_2_warm","harpsichord","lead_7_fifths","lead_6_voice","lead_5_charang","lead_4_chiff","lead_3_calliope","sitar"],
			"bass":["synth_bass_2","synth_bass_1","acoustic_bass","contrabass","slap_bass_1","slap_bass_2"],
			"pianic":["acoustic_grand_piano","accordion","bright_acoustic_piano","honkytonk_piano","electric_piano_2","electric_piano_1","electric_grand_piano","rock_organ","reed_organ","percussive_organ","drawbar_organ","church_organ","dulcimer","harmonica","music_box","tango_accordion"]
		}
	};

}

function StorageInterface(storage, $parent) {
	// Build the actions panel, save, load & new buttons and the name input
	var _this = this,
		$fileActs = $('<div>',{id:'file-actions'}),
			$fileName = $('<input>',{id:'file-name',placeholder:"Tune Name"}),
			$fileSave = $('<button>',{id:'file-save',class:'fa fa-save',title:'Save Tune'}),
			$fileLoad = $('<button>',{id:'file-load',class:'fa fa-file-audio-o',title:'Load Tune'}),
			$fileNew = $('<button>',{id:'file-new',class:'fa fa-plus',title:'New Tune'});

	$fileActs
		.append($fileName)
		.append($fileSave)
		.append($fileLoad)
		.append($fileNew);

	this.model = storage;

	// Accept callbacks from the parent
	var callbacks = {
			load:false,
			save:false,
			new:false,
			getData:false
		};
	this.callbacks = function (cbs) {
		for (var c in cbs)
			callbacks[c] = cbs[c];
	}

	// Set or get the working file name
	this.name = function (name) {
		if (name === undefined)
			return $fileName.val();
		return $fileName.val(name);
	}
	this.new = function () {
		if (callbacks.new)
			callbacks.new();
	}
	this.save = function () {
		var name = $fileName.val();
		if (name) {
			if (! callbacks.getData) return;
			var save = storage.save(name, callbacks.getData());
			// ERR discard auto save
			if (save && callbacks.save)
				callbacks.save(save);
		} else $fileName.focus();
	}
	this.autoSave = function () {
		if (! callbacks.getData) return;
		var save = storage.autoSave($fileName.val(), callbacks.getData());
		if (save && callbacks.autoSave)
			callbacks.autoSave(save);
	}
	this.autoLoad = function () {
		var load = storage.autoLoad();
		if (callbacks.load)
			callbacks.load(load.name, load.file);
	}
	this.load = function () {
		var files = storage.list();

		Files(files,'track',function (name) {
			interface.popup('Loading File...', 'loading');
			var file = storage.load(name);
			if (callbacks.load)
				callbacks.load(name, file);
		});
	}

	// Button events
	$fileNew.click(function () {
		_this.new();
	});
	$fileSave.click(function () {
		_this.save();
	});
	$fileLoad.click(function () {
		_this.load();
	});

	this.view = $fileActs;
	// Append to parent
	if ($parent !== undefined)
		$parent.append($fileActs);
}

function Play(stop) {
	var _this = this,
		playNotes = [],
		events = [],
		falseNotes = {},
		callbacks = {};

	function Event(ev,delay,callback) {
		return window.setTimeout(function () {
			if (callback !== undefined && callback)
				callback(ev[0]);
		},delay);
	}
	function timeDiff(time) {
		return Date.now()-time;
	}
	this.callbacks = function (track, cbs) {
		if (!(track in callbacks))
			callbacks[track] =  {
				noteOn:false,
				noteOff:false,
				wait:false,
				unwait:false,
				start:false,
				stop:false
			};
		for (var c in cbs)
			callbacks[track][c] = cbs[c];
	}
	this.add = function (track, offset, gNotes, gradients, dur) {
		if (offset === undefined)
			offset = track.get('offset')*1000;
		if (gNotes === undefined)
			gNotes = track.get('notes');
		if (gradients === undefined)
			gradients = false;
		var id = track.id(),
			trackNotes = $.extend(true,[],gNotes).reverse(),
			start = track.get('start'),
			stop = track.get('stop');
		if (!(id in falseNotes))
			falseNotes[id] = notes.falseNotes();
		var tmpNotes = notes.falseNotes(),
			lastTime = 0;
		for (var n in trackNotes) {
			var note = trackNotes[n];
			if (note[1] < start || (stop && note[1] > stop))
				continue;

			if (gradients && dur) {
				var x = (dur.s+(note[1]-start))/dur.d;

				if (x < 0) x = 0;
				if (x > 1) x = 1;
				if ('volume' in gradients) {
					var y = gradients.volume.getAtX(x);
					if (y) {
						y = y.y;
						if (y < 0) y = 0;
						if (y > 1) y = 1;
						note[3] = Math.round(y*255);
					}
				}
				if ('pitch' in gradients) {
					var y = gradients.pitch.getAtX(x);
					if (y) {
						y = y.y;
						if (y < 0) y = 0;
						if (y > 1) y = 1;

						var n = Math.round(y*notes.midiCount());
						note[2] = MIDI.noteToKey[(MIDI.keyToNote[note[2]]-track.getPitch())+n];
					}
				}
				if ('offset' in gradients) {
					var y = gradients.offset.getAtX(x);
					if (y) {
						y = y.y;
						if (y < 0) y = 0;
						if (y > 1) y = 1;
						y = (y-0.5)*2;

						var n = Math.round(y*track.getNoteDuration());

						note[1] = note[1] + n;
						// var n = Math.round(y*notes.midiCount());
						// note[2] = MIDI.noteToKey[(MIDI.keyToNote[note[2]]-track.getPitch())+n];
					}
				}
			}
			var time = Math.round(note[1] + offset - start);
			note[1] = time;

			tmpNotes[note[2]] = note[0];
			// if (time > lastTime) lastTime = time;

			note.unshift(id);


			// Sort into current notes
			var found = false;
			for (var i in playNotes) {
				stime = playNotes[i][2];
				if (stime < time) {
					playNotes.splice(i,0,note);
					found = true;
					break;
				}
			}
			if (! found)
				playNotes.push(note);
		}
		// ensure all notes are turned off before at end
		for (var t in tmpNotes) {
			if (tmpNotes[t]) {
				var time = track.getDuration() + offset,
					note = [id,false,time,t];

				// Sort into current notes
				var found = false;
				for (var i in playNotes) {
					stime = playNotes[i][2];
					if (stime < time) {
						playNotes.splice(i,0,note);
						found = true;
						break;
					}
				}
				if (! found)
					playNotes.push(note);
			}
		}


		/* weed out duplicate events
		assume previous events have been added first
			remove stop at start
			remove wait at start
				move to next stop

				  aaa						bbbbbb
		wait start   stop		unwait start  ss   stop
			start    stop wait	unwait start  sta  stop

		*/
		function findEv(time,ev) {
			for (var v in events) {
				var EV = events[v];
				if (EV[0] == id &&
					EV[1] == time &&
					EV[2] == ev)
					return EV;
			}
			return false;
		}
		function lastEv(time,ev) {
			for (var v in events) {
				var EV = events[v];
				if (EV[0] ==  id &&
					EV[1] < time &&
					EV[2] == ev)
					return EV;
			}
			return false;
		}
		// events: wait unwait start stop
		var roundEnd = Math.round(offset+track.getDuration());
		if (! findEv(roundEnd,'start'))
			events.push([id,roundEnd,'stop']);
		offset = Math.round(offset);
		if (offset) {
			if (findEv(0,'start') || findEv(0,'wait')) {
				var last = lastEv(offset,'stop');
				if (last)
					events.push([id,last[1],'wait']);
			} else events.push([id,0,'wait']);

			var wt = findEv(offset,'wait');
			if (wt)
				events.splice(events.indexOf(wt),1);

			var st = findEv(offset,'stop');
			if (st)
				events.splice(events.indexOf(st),1);

			events.push([id,offset,'start']);
			events.push([id,offset,'unwait']);
		} else {
			events.push([id,0,'start']);
		}
		events.sort(function (a,b) {
			var d = b[1]-a[1];
			if (!d) {
				// stop before start
				if (b[2] == 'stop')
					return -1;
				if (a[2] == 'stop')
					return 1;
			}
			return d;
		});
	}
	var intervals = [],
		playing = false;
	this.start = function () {
		if (playing) return;
		playing = true;
		console.log('start playing '+playNotes.length+' notes')
		var progress = 0,
			step = 500;
		var playStep = function () {
			var endStep = progress + step,
				lastTime = 0,
				stopPos = false;

			// console.log(timeDiff(startTime))
			intervals.push(window.setTimeout(function () {
				playStep()
			}, step+progress-timeDiff(startTime)));


			for (var p=playNotes.length-1;p>=0;p--) {
				var note = playNotes[p],
					time = note[2];
				if (time <= endStep) {
					var track = note[0],
						key = note[3],
						delay = time-timeDiff(startTime);
					if (note[1]) {
						if (callbacks[track].noteOn)
							falseNotes[track][key] = callbacks[track].noteOn(key, track, delay, note[4]);
					} else {
						if (callbacks[track].noteOff) {
							callbacks[track].noteOff(key, track, delay);
							falseNotes[track][key] = false;
						}
					}
					if (time > lastTime)
						lastTime = time;
					// Would it be faster to index cut off?
					playNotes.splice(p,1);

				}
				// ERR may be buggy if notes are out of order
				else break;
			}
			for (var p=events.length-1;p>=0;p--) {
				var ev = events[p],
					time = ev[1];
				if (time <= endStep) {
					intervals.push(Event(ev,time-timeDiff(startTime),callbacks[ev[0]][ev[2]]));
					events.splice(p,1);
				}
				else break;
			}
			if (playNotes.length == 0) {
				for (var i in intervals)
					window.clearTimeout(intervals[i]);
				intervals = [];
				intervals.push(window.setTimeout(function () {
					_this.stop();
				}, (lastTime-progress)));
				return;
			}
			progress += step;
		};
		var startTime = Date.now();
		playStep();
	}
	this.stop = function () {
		if (!playing) return;
		playing = false;
		console.log('stop playing')
		for (var i in intervals)
			window.clearTimeout(intervals[i]);
		intervals = [];
		interval = false;
		for (var t in falseNotes) {
			for (var n in falseNotes[t]) {
				var note = falseNotes[t][n];
				if (note) {
					if (note instanceof Array) {
						for (var n in note)
							note[n].stop();
					} else note.stop();
				}
			}
		}
		if (stop !== undefined)
			stop();
	}
}

function Tracks($actionPar, $parent) {
	// Collection and interface for track collection

	var _this = this, // Maintaining scope again
		Tracks = {}, // Define the track collection
		TrackIds = 0, // Counter to keep track of ids used
		currentTrack = 0, // Curently active track
		recording = false;
	// Build the track actions and list  elements
		$trackActions = $('<div>',{id:'track-actions'})
			$play = $('<button>',{id:'play',class:'fa fa-play'}),
			$edit = $('<button>',{id:'edit',class:'fa fa-pencil'}),
		$container = $('<div>',{id:'track-container'})
			$tracks = $('<ol>',{id:'tracks'}),
	// Define the callbacks foreach track
		trackCallbacks = {
			stopRecording:function () {
				_this.stop(); // Stop any playing tracks
				_this.spareTrack(); // Create a new blank track if possible
				interface.pulse('stop'); // Notify the user of stop
				recording = false;
				interface.storage.autoSave();
			},
			update:function (model) {
				_this.sequencer.sequenceUpdate(model);
				interface.storage.autoSave();
			},
			startRecording:function () {
				_this.stop(); // Stop any playing tracks
				_this.start(true); // Start tracks without offset
				interface.pulse('circle'); // Notify the user of recording
				recording = true;
			},
			// end:function (repeat) {
			// 	// Count those which have ended and trigger a stop is all are done
			// 	if (repeat == true)
			// 		_this.countPlay(0);
			// 	else _this.countPlay(1);
			// },
			active: function (track) {
				// Deactivate all other tracks then activate this one
				_this.activeTrack(track);
			}
		},
	// Accept callbacks from parent
		callbacks = {
			start:false,
			stop:false,
			active:false,
			noteOn:false,
			noteOff:false
		};
	this.callbacks = function (cbs) {
		for (var c in cbs)
			callbacks[c] = cbs[c];
	}

	$container.append($tracks);

	this.stop = function (stopRec) {
		if (stopRec == undefined) stopRec = false;
		// Stop all tracks
		for (var t in Tracks) {
			var track = Tracks[t];
			track.model.stop();
			if (stopRec && track.isRecording() && ! recording)
				track.stopRecording();
		}
		$play.removeClass('active');
		if (callbacks.stop)
			callbacks.stop();
	}
	this.start = function (skipOffset) {
		// Start all tracks
		var play = new Play(_this.stop),
			pass = {play:play,callback:false};
		if (skipOffset)
			pass.offset = 0;
		for (var t in Tracks) {
			Tracks[t].model.stop();
			Tracks[t].model.start(pass);
		}
		play.start();
		$play.addClass('active');
		if (callbacks.start)
			callbacks.start(play);
	}

	this.getAll = function () {
		// Return track collection
		return Tracks;
	}
	this.reBuild = function (tracks) {
		// Re-build track list display with new tracks
		Tracks = {};
		TrackIds = 0;
		currentTrack = 0;
		$edit.removeClass('active');
		$tracks.empty();
		this.sequencer.reBuild();
		for (var t in tracks)
			this.addTrack(tracks[t]);
		this.spareTrack();
		this.activeTrack(Tracks[0]);
	}
	this.spareTrack = function () {
		// Create a new blank track so long as there are less that 16 (max MIDI tracks)
		var tl = Object.keys(Tracks).length;
		if (this.recordCount() == tl)// && tl<16)
			this.addTrack({tempo:this.commonTempo()});
	}
	this.commonTempo = function () {
		var tmps = {};
		for (var t in Tracks) {
			var tmp = Tracks[t].model.get('tempo');
			if (!(tmp in tmps)) tmps[tmp] = 0;
			tmps[tmp] += 1;
		}
		var maxTmp = false;
		for (var t in tmps) {
			if ((! maxTmp) || tmps[t] > tmps[maxTmp])
				maxTmp = t;
		}
		if (maxTmp)
			return maxTmp;
		else return 100;
	}
	this.addTrack = function (track) {
		// Create a new track and its model, define intial properties by passing the track object
		var trackModel = new Track(TrackIds, track);
		Tracks[TrackIds] = new TrackInterface(trackModel, $tracks);
		Tracks[TrackIds].callbacks(trackCallbacks);
		trackModel.callbacks({
			noteOn:this.noteOn,
			noteOff:this.noteOff
		})
		TrackIds += 1;
		this.sequencer.addTrack(trackModel);

	}
	this.activeTrack = function (track) {
		// Activate the given track, deactivating the others
		if (grid != 'falsy')
			return;
		if (track === undefined)
			track = Tracks[currentTrack];
		if (track instanceof TrackInterface)
			track = track.model;
		var id;
		if (track instanceof Track)
			id = track.id();
		else {
			id = track;
			track = Tracks[id].model;
		}

		var cont = this.sequencer.activeTrack(track);
		if (! cont) return;
		for (var t in Tracks)
			Tracks[t].deactivate();
		Tracks[id].activate();
		currentTrack = id;
		if (callbacks.active)
			callbacks.active(track);
	}
	this.currentTrack = function (track) {
		// Return current track interface model (*no args*)
		if (track === undefined)
			return Tracks[currentTrack];
		// Or return weather or not the given track is currently active
		if (track instanceof TrackInterface)
			track = track.model;
		if (track instanceof Track)
			track = track.id();
		return currentTrack == track;
	}

	this.record = function (on,note) {
		// Record note to actively recording track
		for (var t in Tracks) {
			var track = Tracks[t];
			if (track.isRecording()) {
				track.record(on,note);
				break;
			}
		}
	}
	this.recordCount = function () {
		// Count the number of tracks with recordings
		var ret = 0;
		for (var t in Tracks) {
			if (Tracks[t].model.get('notes').length)
				ret += 1;
		}
		return ret;
	}
	this.isRecording = function () {
		return recording;
		for (var t in Tracks) {
			if (Tracks[t].isRecording())
				return Tracks[t];
		}
		return false;
	}

	this.noteOn = function (note, track, delay, volume) {
		// Switch a note off using MIDI

		if (delay === undefined)
			delay = 0;
		if (track === undefined) {
			track = currentTrack;
			if ((!Tracks[track].model.get('notes').length) && (! Tracks[track].isRecording()) && (!this.isRecording())) {
				Tracks[track].startRecording();
			}
			this.record(true, note);
		}
		if (track instanceof TrackInterface)
			track = track.model;
		if (track instanceof Track)
			track = track.id();
		if (! Tracks[track].model.get('muted')) {
			//Auto record
			// if not recorded and not recording any
			if (volume === undefined)
				volume = Tracks[track].model.get('volume');


			var ret = instruments.notesOn(note,Tracks[track].model.get('instrument'),volume,delay);

			if (callbacks.noteOn)
				callbacks.noteOn(note, track, delay);
			return ret;
		}
	}
	this.noteOff = function (note, track, delay) {
		// Switch a note on using MIDI
		if (delay === undefined)
			delay = 0;
		if (track === undefined) {
			track = currentTrack;
			this.record(false, note);
		}
		if (track instanceof TrackInterface)
			track = track.model;
		if (track instanceof Track)
			track = track.id();

		instruments.notesOff(note,Tracks[track].model.get('instrument'),delay);


		if (callbacks.noteOff)
			callbacks.noteOff(note, track, delay);
	}

	var grid = 'falsy';
	$edit.click(function () {
		if ($(this).is('.active')) {
			grid.close();
		} else {
			var trackView =_this.currentTrack();
			trackView.editing(true);
			grid = new Grid(trackView, trackView.view);
			trackView.model.editing = grid;
			$tracks.scrollTop(trackView.view.position().top+$tracks.scrollTop());
			$tracks.scroll(function () {
				var pt = trackView.view.position().top;
				if (Math.abs(Math.round(pt/10)) <= 1) {
					$(this).scrollTop(pt +$tracks.scrollTop());
					return false;
				}

			})
			grid.callbacks({
				close:function () {
					$edit.removeClass('active');
					trackView.editing(false);
					trackView.model.editing = false;
					$tracks.unbind('scroll');
					grid = 'falsy';
					// Re-display track notes
					trackView.displayNotes();
					_this.spareTrack();
				},
				save:function () {
					if (trackView.model.get('notes').length)
						trackView.view.addClass('recorded');
					else trackView.view.removeClass('recorded');

					trackCallbacks.update(trackView.model);
				},
				scroll:function (left) {
					trackView.view.find('.ticker').css({marginLeft:130-left});
				}
			})
			$(this).addClass('active');
		}
	})

	$play.click(function () {
		if ($(this).is('.active')) {
			recording = false;
			_this.stop(true);
		} else {
			_this.start();
		}
	});


	// Append to parent
	if ($parent !== undefined)
		$parent.append($container);

	// Use separate toolbar parent
	if ($actionPar !== undefined) {
		$actionPar
			.append($trackActions
				.append($play)
				.append($edit));
	}

	this.sequencer = new Sequencer(this);
	$container.prepend(this.sequencer.view);

	// Add a default blank track
	this.addTrack();


}

function Track(id, props) {
	var _this = this,
		step = 500,
		properties = {notes:[],instrument:[0],start:0,stop:0,loop:1,offset:0,muted:false,tempo:100,bar:4,sequence:[],volume:127},
		playing = 'falsy',
		callbacks = {
			start:false,
			stop:false,
			step:false,
			noteOn:false,
			noteOff:false,
			wait:false,
			unwait:false
		};
	this.callbacks = function (cbs) {
		for (var c in cbs)
			callbacks[c] = cbs[c];
	}

	this.id = function () {
		return id;
	}

	this.getPitch = function () {
		return notes.noteToMidi(properties.notes[0][2]);
	}
	this.get = function (prop) {
		if (prop === undefined || ! (prop in properties))
			return;
		return properties[prop];
	}
	this.set = function (prop, val) {
		if (typeof prop == 'object') {
			for (var p in prop)
				this.set(p, prop[p]);
		} else if (prop in properties) {
			if (prop == 'instrument') {
				// this.setInstrument(val);
				if (typeof val == 'string')
					val = instruments.get(val);
				else if (val instanceof Array) {
					for (var v in val) {
						if (typeof val[v] == 'string')
							val[v] = instruments.get(val[v]);
					}
				}
			}
			properties[prop] = val;
		}
	}

	this.getNoteDuration = function () {
		// properties.tempo
		return 60*1000/properties.tempo;
	}
	this.getDuration = function () {
		if (! properties.notes.length)
			return 0;
		var stop,start=properties.start;
		if (properties.stop)
			stop = properties.stop;
		else stop = properties.notes[properties.notes.length-1][1];
		return stop-start;
	}
	this.setProgram = function (instrument) {
		return;
		if (typeof instrument != 'string')
			instrument = instruments.get(instrument);
		MIDI.programChange(id, MIDI.GM.byName[instrument].number);
	}

	this.setInstrument = function (instrument) {
		if (typeof instrument != 'string')
			instrument = instruments.get(instrument);
		MIDI.setInstrument(id, MIDI.GM.byName[instrument].number);
	}

	function timeDiff(time) {
		return Date.now()-time;
	}
	this.editing = false;
	this.stop = function(pass) {
		if (playing == 'falsy')
			return;
		playing.stop();
		playing = 'falsy';
		if (callbacks.stop)
			callbacks.stop(pass);
	}
	this.start = function (dat) {
		if (properties.muted || (! properties.notes.length))
			return false;
		playing = true;
		callback = false;
		if (! ('play'in dat)) {
			dat.play = new Play(_this.stop);
			callback = true;
		}
		dat.play.callbacks(id, callbacks);
		var defs = {
			offset:properties.offset*1000,
			loops:properties.stop ? properties.loop : 1
		},
		opts = $.extend(defs,dat);

		playing = opts.play;
		var dur = this.getDuration();
		var pNotes = undefined;
		if (_this.editing)
			pNotes = _this.editing.notes();
		var grds = {};
		if (dat.gradients !== undefined) {
			for (var g in dat.gradients) {
				grds[g] = new Paths(gradDefs,dat.gradients[g]);
			}
		}
		for (var r=0;r<opts.loops;r++)
			opts.play.add(this,opts.offset+(r*dur),pNotes,grds,{s:(r*dur),p:dur,d:dur*opts.loops});
		if (callback)
			opts.play.start();
		return true

	}

	if (props !== undefined)
		this.set(props);
}

function TrackInterface(track, $parent) {
	var _this = this,
		timeFactor = 50,
		record = 'falsy',
		$track = $('<li>',{class:'track','data-track':track.id()}),
			$actions = $('<div>',{class:'actions'}),
				$record = $('<button>',{class:'record fa fa-circle'}),
				$play = $('<button>',{class:'play fa fa-play'}),
				$vol = $('<input>',{type:'number',min:0,max:255,value:track.get('volume')}),
			$notes = $('<div>',{class:'notes'}),
			$ticker = $('<div>',{class:'ticker'}),
			$start = $('<div>',{class:'marker start'}),
				$startIn = $('<input>',{value:track.get('offset'),class:'marker-input offset', type:'number',min:0}),
			$stop = $('<div>',{class:'marker stop'}),
				$stopIn = $('<input>',{value:track.get('loop'),class:'marker-input loop', type:'number',min:1,step:1});

	$track
		.append($actions
			.append($record)
			.append($play)
			.append($vol))
		.append($notes)
		.append($ticker)
		.append($start
			.append($startIn))
		.append($stop
			.append($stopIn))

	this.model = track;
	this.view = $track;

	var timeout = false,
		ticking = false;

	track.callbacks({
		start:function () {
			var start = track.get('start'),
				dur = track.getDuration();
			$ticker.css('left',timePos(start));
			$play.addClass('active');
			$ticker.stop(true,false).animate({
				'left':timePos(start+dur)
			},dur,"linear");
			ticking = [start,dur,Date.now()];
		},
		wait:function () {
			$track.addClass('waiting');
		},
		unwait:function () {
			$track.removeClass('waiting');
		},
		step:function (start, step, end) {
			$ticker.stop(true,false).animate({
				'left':timePos(end)
			},step,"linear");
		},
		stop:function (repeat) {
			if (timeout)
				window.clearTimeout(timeout);
			timeout = false;
			ticking = false;
			$ticker.stop(true,false).css('left',0);
			$track.removeClass('waiting');
			$play.removeClass('active');
			if (callbacks.end)
				callbacks.end(repeat);
		}
	});

	var callbacks = {
		startRecording:false,
		stopRecording:false,
		end:false,
		active:false,
		update:false
	};
	this.callbacks = function (cbs) {
		for (var c in cbs)
			callbacks[c] = cbs[c];
	}

	this.isRecording = function () {
		return record != 'falsy';
	}
	this.record = function (on, note) {
		record.record(on,note);
	}
	this.stopRecording = function () {
		delete record;
		record = 'falsy';
		$record.removeClass('active');
	}
	this.startRecording = function () {
		$record.addClass('active')
		record = new Recording(track);
	}
	this.events = function () {
		$track.click(function (ev) {
			if($(ev.target).is('.play'))
				return;
			if (callbacks.active)
				callbacks.active(track);
		})
		.dblclick(function (ev) {
			var $target = $(ev.target);
			if ($target.is('.marker') || editing)
				return;
			if ($(this).is('.recorded')) {
				$(this).toggleClass('muted');
				track.set('muted', $(this).hasClass('muted'));
			}
		})

		$record.click(function () {
			if ($track.is('.active:not(.muted)')) {
				if ($record.is('.active')) {
					console.log('stop recording')

					var saved = record.save();
					if (saved) {
						track.set('notes', saved);
						_this.displayNotes();
						$track.addClass('recorded');
						if (callbacks.update)
							callbacks.update(track);
					}

					_this.stopRecording();
					if (callbacks.stopRecording)
						callbacks.stopRecording();
				} else {
					console.log('recording')

					track.set('muted', true);
					if (callbacks.startRecording)
						callbacks.startRecording();
					track.set('muted', false);

					_this.startRecording();
				}
			}
		});
		$play.click(function () {
			if ($play.is('.active'))
				track.stop();
			else track.start({offset:0});
		});
		$vol.change(function () {
			track.set('volume',$(this).val()*1);
		})

		$track.find('.marker')
		.dblclick(function() {
			var $marker = $(this);
			$marker.toggleClass('active');
			if ($marker.is('.active'))
				$marker.find('input').focus();
		})
		.draggable({
			axis:'x',
			containment:'parent',
			stop: function () {
				$track.find('.time-marker').remove();
				var btime = 60*1000/track.get('tempo'),
					time = posTime($(this).position().left),
					rtime = Math.round(time/btime)*btime;
				if ($(this).is('.start'))
					track.set('start', rtime);
				if ($(this).is('.stop'))
					track.set('stop',rtime);

				if (callbacks.update)
					callbacks.update(track);
			},
			drag:function (ev, ui) {
				if ($(this).is('.stop')) {
					var start = $start.position().left,
						$marker = $track.find('.time-marker');
					var btime = 60*1000/track.get('tempo'),
						time = posTime(ui.position.left),
						bars = Math.round(time/btime),
						rtime = bars*btime;
					ui.position.left = timePos(rtime);
					if (! $marker.length) {
						$marker = $('<div>',{class:'time-marker'});
						$track.append($marker);
					}
					$marker.html(bars).css({
						left:start,
						width:ui.position.left-start
					});
				} else
				if ($(this).is('.start')) {
					var end = $(this).position().left,
						$marker = $track.find('.time-marker');
					if (! $marker.length) {
						$marker = $('<div>',{class:'time-marker'});
						$track.append($marker);
					}
					$marker.html(posTime(end)/1000).css({
						left:0,
						width:end
					});
				}
			}
		});
		$track.find('.marker input').keypress(function(ev) {
			var k = getKey(ev);
			if (k == 13) {
				$(this).blur()
				.closest('.marker').removeClass('active');
			} /*else if (k == 27)
				$(this)
				.closest('.marker').removeClass('active');*/
		}).change(function () {
			if ($(this).is('.offset'))
				track.set('offset', $(this).val()*1);
			if ($(this).is('.loop'))
				track.set('loop', $(this).val()*1);

			if (callbacks.update)
				callbacks.update(track);
		});
	}

	var editing = false;
	this.editing = function (ed) {
		if (ed === undefined)
			return editing;
		editing = ed;
		if (editing) {
			$track.addClass('editing');
			$ticker.css('margin-left',130)
			timeFactor = 20;
		} else {
			$track.removeClass('editing');
			$ticker.css('margin-left',90)
			timeFactor = 50;
		}
		if (ticking) {
			var td = Date.now()-ticking[2],
				start = ticking[0],
				dur = ticking[1];
			$ticker.stop(true,false).css({
				left:timePos(start+td)
			}).animate({
				'left':timePos(start+dur)
			},dur-td,"linear");
		}
	}



	this.displayNotes = function() {
		var notesActive = notes.falseNotes();
		$notes.empty();
		var trackNotes = track.get('notes');

		for (var n in trackNotes) {
			var note = trackNotes[n];
			if (note[0]) {
				notesActive[note[2]] = note[1];
			} else {
				if (notesActive[note[2]]) {
					$notes.append($('<div>',{class:'note'}).css({
						width:timePos(note[1] - notesActive[note[2]]),
						left:timePos(notesActive[note[2]]),
						bottom:(notes.noteToMidi(note[2])/(notes.midiCount()+1))*100+"%"
					}));
					notesActive[note[2]] = false;
				}
			}
		}
	}
	function timePos (a) {
		return Math.round(a/timeFactor);
	}
	function posTime (a) {
		return Math.round(a)*timeFactor;
	}


	var active = false;
	this.deactivate = function () {
		active = false;
		$track.removeClass('active');
	}
	this.activate = function () {
		active = true;
		$track.addClass('active');
	}



	if ($parent !== undefined)
		$parent.append($track);



	$start.css('left', timePos(track.get('start')));
	$stop.css('left', timePos(track.get('stop')));
	if (track.get('muted'))
		$track.addClass('muted');
	if (track.get('notes').length) {
		$track.addClass('recorded');
		this.displayNotes();
	}


	this.events();
}



function Recording(track) {
	var recordNotes = [],
		startTime = Date.now();
	this.record = function (on, note) {
		var t = Date.now()-startTime;
		if (t < 1) t=1;
		recordNotes.push([on, t, note]);
	}
	this.save = function () {
		if (recordNotes.length)
			return recordNotes;
		return false;
	}

}



function Instruments() {

	var _this = this,
		instruments = [
		'acoustic_grand_piano',
		// 'accordion',
		// 'acoustic_bass',
		// 'acoustic_guitar_nylon',
		// 'acoustic_guitar_steel',
		// 'banjo',
		// 'cello',
		// 'church_organ',
		// 'clarinet',
		// 'distortion_guitar',
		// 'flute',
		// 'trumpet',
		// 'violin',
		// 'trombone',
		// 'synth_drum',
	],
	all_instruments = [],
	working_instruments = [],
	callbacks = {
		load:false,
		loaded:false
	};
	this.callbacks = function (cbs) {
		for (var c in cbs)
			callbacks[c] = cbs[c];
	}

	this.isLoaded = function (instrument) {
		return instruments.indexOf(instrument) != -1;
	}
	this.loaded = function (loaded) {
		if (loaded === undefined)
			return instruments;
		instruments.push(loaded);
	}
	this.unloaded = function () {
		var ret = [];
		for (var i in all_instruments) {
			var instrument = all_instruments[i];
			if (instruments.indexOf(instrument) == -1)
				ret.push(instrument);
		}
		return ret;
	}
	this.available = function (set) {
		if (set === undefined)
			return all_instruments;
		else all_instruments = set;
	}

	this.get = function (instrument) {
		if (typeof instrument == 'string')
			return instruments.indexOf(instrument);
		else return instruments[instrument];
	}

	this.notesOff = function (note,ins,delay) {
		for (var i in ins) {
			MIDI.noteOff(working_instruments.indexOf(ins[i]), MIDI.keyToNote[note], delay/1000);
		}
	}
	this.notesOn = function (note,ins,volume,delay) {
		var ret = [];
		for (var i in ins) {
			ret.push(MIDI.noteOn(working_instruments.indexOf(ins[i]), MIDI.keyToNote[note], volume, delay/1000));
		}
		return ret;
	}
	this.working = function (tracks) {
		var ts = tracks.getAll();
		var ret = [];
		for (var t in ts) {
			var track = ts[t],
				ins = track.model.get('instrument');
			for (var I in ins) {
				var i = ins[I];
				if (ret.indexOf(i) == -1)
					ret.push(i);
			}
		}
		working_instruments = ret;
		for (var w in working_instruments) {
			var instrument = working_instruments[w];
			if (typeof instrument != 'string')
				instrument = this.get(instrument);
			MIDI.programChange(w, MIDI.GM.byName[instrument].number);
		}
		console.log(ret.length+" working instruments");
	}


	this.loadInstrument = function (instrument, callback) {
		if (callbacks.load)
			callbacks.load();
		MIDI.loadResource({
			instrument:instrument,
			onsuccess:function () {
				_this.loaded(instrument);
				if (callbacks.loaded)
					callbacks.loaded(instrument);
				if(callback !== undefined)
					callback();
			}
		})
	}

}

function InstrumentInterface(instruments, $parent) {
	var _this = this,
		$actions = $('<div>',{id:'instrument-actions'}),
			$instruments = $('<select>',{id:'instruments',multiple:'multiple'}),
			$addInstrument = $('<button>',{id:'add-instrument',class:'fa fa-ellipsis-h',title:'Load More Instruments'});

	this.model = instruments;

	var callbacks = {
			change:false
		};
	this.callbacks = function (cbs) {
		for (var c in cbs)
			callbacks[c] = cbs[c];
	}

	this.display = function () {
		// Display the loaded instruments in the instriments select box
		var val = $instruments.val();
		$instruments.empty();
		var loaded = instruments.loaded();
		for (var i in loaded) {
			var instrument = loaded[i];
			$instruments.append($('<option>',{value:instrument}).html(instrument.replace(/[\_]+/g," ")))
		}
		$instruments.val(val);
	}
	this.selectInstrument = function (instrument, change) {
		// Select the given instrument from the list
		// Trigger a change if required
		if (change === undefined) change = false;
		if (instrument instanceof Array) {
			var ret = [];
			for (var i in instrument) {
				if (typeof instrument[i] != 'string')
					ret[i] = instruments.get(instrument[i]);
				else ret[i] = instrument[i];
			}
			instrument = ret;
		} else
		if (typeof instrument != 'string')
			instrument = instruments.get(instrument);
		$instruments.val(instrument);
		if (change)
			$instruments.change();
	}
	$actions
		.append($instruments)
		.append($addInstrument);


	function addInstrument() {
		// Create instrument selection menu
		var unloaded = instruments.unloaded();
		Files(unloaded,'instrument',function (instrument) {
			loadInstrument(instrument);
		});
	}
	function loadInstrument (instrument) {
		// Load selected instrument through the instruments model
		interface.popup('Loading Instrument...', 'loading');
		instruments.loadInstrument(instrument, function () {
			interface.popup();
		});
	}

	// Once loaded, re-display the list and select the new instrument
	instruments.callbacks({
		loaded:function (instrument) {
			_this.display();
			_this.selectInstrument(instrument, true);
		}
	})

	// Interaction events
	$addInstrument.click(addInstrument);
	$instruments.change(function () {
		if (callbacks.change)
			callbacks.change($(this).val());
		$(this).blur();
	});

	this.view = $actions;
	// Append to given element
	if ($parent !== undefined)
		$parent.append($actions);
}



function Sequencer(tracks, $parent) {
	var _this = this,
		$sequencer = $('<div>',{id:'sequencer',class:'hidden'}),
			$toggle = $('<div>',{id:'sequencer-toggle',class:'fa fa-th-large'}).html(' Sequencer'),
			$ticker = $('<div>',{id:'sequencer-ticker'}),
			$actions = $('<div>',{id:'sequencer-actions'}),
				$play = $('<button>',{id:'sequencer-play',class:'fa fa-play play'}),
				$edit = $('<button>',{id:'sequencer-edit',class:'fa fa-pencil edit'}),
			$tracks = $('<ol>',{id:'sequencer-tracks'}),
		Stracks = {},
		grads = false;
	$sequencer
		.append($toggle)
		.append($ticker)
		.append($actions
			.append($play)
			.append($edit))
		.append($tracks);

	var playing = 'falsy';
	this.start = function () {
		var mdur = 0;
		for (var t in Stracks) {
			var dur = Stracks[t].duration();
			if (dur > mdur)
				mdur = dur;
		}
		$ticker.stop(true,false);
		$play.addClass('active');
		var play = new Play(_this.stop);
		// play.callbacks({
		// 	start:function
		// })
		for (var t in Stracks)
			Stracks[t].start(play);
		play.start();
		$ticker.animate({left:mdur/100},mdur,'linear');
	}
	this.stop = function () {
		for (var t in Stracks)
			Stracks[t].stop();
		$ticker.stop(true,false).css({left:0});
		$play.removeClass('active');
	}
	this.reBuild = function () {
		$tracks.empty();
		Stracks = {};
		var tList = tracks.getAll()
		for (var t in tList) {
			var track = tList[t].model;
			this.addTrack(track);
		}
	}
	this.addTrack = function (track) {

		var Strack = new SequencerTrack(track, $tracks);
		Stracks[track.id()] = Strack;
		Strack.callbacks({
			activate:function (track) {
				tracks.activeTrack(track);
			},
			resize:function (block) {
				if (! grads) return;
				grads.resize(block.innerWidth()-(gradDefs.padding*2),block.innerHeight()-20-(gradDefs.padding*2));
			}
		});
	}
	this.sequenceUpdate = function (track) {
		Stracks[track.id()].sequenceUpdate();
	}
	this.activeTrack = function (track) {
		if (grads) return false;
		$tracks.find('.sequencer-track.active').removeClass('active');
		$tracks.find('.sequencer-track[data-track='+track.id()+']').addClass('active');
		return true;
	}
	this.reBuild();

	function editSeqTrack() {
		var trackView = tracks.currentTrack();
		var TV = Stracks[trackView.model.id()],
			block = TV.currentBlock();
		if (! block) return false;
		var Grads = block.data('gradients');


		TV.view.addClass('editing');
		$tracks.scrollTop(TV.view.position().top+$tracks.scrollTop()-$tracks.position().top);
		$tracks.scrollLeft(block.position().left);
		function scrollGrad() {
			var pt = TV.view.position().top-$tracks.position().top,
			lt = block.position().left-$tracks.scrollLeft(),
			ltf = block.position().left+block.outerWidth()-$tracks.scrollLeft()-$tracks.innerWidth();
			var ret = false;
			if (Math.abs(Math.round(pt/10)) <= 1) {
				$(this).scrollTop(pt +$tracks.scrollTop());
				ret = true;
			}
			if (Math.abs(Math.round(lt/20)) <= 1) {
				$(this).scrollLeft(lt +$tracks.scrollLeft());
				ret = true;
			}

			if (Math.abs(Math.round(ltf/20)) <= 1) {
				$(this).scrollLeft(block.position().left-$tracks.innerWidth()+block.outerWidth());
				ret = true;
			}
			if (ret)
				return false;
		}
		$tracks.scroll(scrollGrad);
		block.addClass('editing');
		if (Grads === undefined) Grads = {};
		for (var g in gradProps) {
			if (!(gradProps[g] in Grads)) {
				if (gradProps[g] == 'volume') {
					var y = trackView.model.get('volume')/255;
					Grads[gradProps[g]] = [{s:{x:0,y:y},e:{x:1,y:y}}];
				}
				else if (gradProps[g] == 'pitch') {
					var y = trackView.model.getPitch()/notes.midiCount();
					Grads[gradProps[g]] = [{s:{x:0,y:y},e:{x:1,y:y}}];
				} else if (gradProps[g] == 'offset') {

					Grads[gradProps[g]] = [{s:{x:0,y:0.5},e:{x:1,y:0.5}}];
				}
				else Grads[gradProps[g]] = [];
			}
		}
		// trackView.editing(true);
		// trackView.model.editing = grid;
		grads = new Gradients(gradDefs,Grads,block);
		function resizeGrad() {
			grads.resize(block.innerWidth()-(gradDefs.padding*2),block.innerHeight()-20-(gradDefs.padding*2));
		}
		var $saveGrads = $('<button>',{class:'fa fa-check'});
		$saveGrads.insertAfter(grads.view.find('select'));
		$saveGrads.click(function(){
			block.data("gradients",grads.getGradients())
			grads.close();
			TV.updateSequence();

		})
		$(window).resize(resizeGrad);
		grads.callbacks({
			close:function () {
				$tracks.unbind('scroll',scrollGrad);
				$edit.removeClass('active');
				block.removeClass('editing');
				TV.view.removeClass('editing');
				$(window).unbind("resize",resizeGrad);
				grads = false;
			},
			draw:function (config) {
				var n = block.find('input').val()*1;
				config.canvas.ctx.beginPath();
				for (var i=0;i<=n;i++) {
					var x = i*config.width/n;
					config.canvas.ctx.moveTo(x,0);
					config.canvas.ctx.lineTo(x,config.height);
				}
				config.canvas.ctx.strokeStyle = "#ccc";
				config.canvas.ctx.stroke();

			}
		});
		resizeGrad();
		return true;
	}
	$toggle.click(function () {
		$sequencer.toggleClass('hidden');
		$(window).resize();
	})

	$play.click(function () {
		if ($play.is('.active')) {
			_this.stop();
		} else {
			_this.start();
		}
	})
	$tracks.scroll(function () {
		$ticker.css({marginLeft:50-$(this).scrollLeft()})
	});
	$edit.click(function () {
		if ($(this).is('.active')) {
			grads.close();
		} else {
			var cont = editSeqTrack();
			if (!cont) return;
			$(this).addClass('active');
		}
	})

	this.view = $sequencer;
	if ($parent !== undefined)
		$parent.append($sequencer);
}

function SequencerTrack(track, $parent) {
	var _this = this,
		dur,bpm,bpb,
		dfact,btime,bwidth,dwidth;
	var selectedBlock = false;

	var callbacks = {
			activate:false,
			resize:false
		};
	this.callbacks = function (cbs) {
		for (var c in cbs)
			callbacks[c] = cbs[c];
	}
	var timeFactor = 100;
	function posTime (pos) {
		return Math.round(pos*timeFactor);
	}
	function timePos(time) {
		return Math.round(time/timeFactor);
	}

	var $track = $('<li>',{class:'sequencer-track','data-track':track.id()});

	this.currentBlock = function () {
		return selectedBlock;
	}
	this.duration = function () {
		var $blocks = $track.find('.sequence-block'),
			mt = 0;
		$blocks.each(function () {
			var lt = Math.round($(this).position().left/bwidth)*btime,
				wt = $(this).find('.sequence-loop-input').val()*1*dur,
				ft = lt+wt;
			if (ft > mt)
				mt = ft;
		})
		return mt;
	}
	this.stop = function () {
		track.stop();
	}
	this.start = function (play) {
		this.stop();
		var sequence = track.get('sequence');
		var start = Date.now();
		for (var s in sequence) {
			var block = sequence[s];
			track.start({play:play,loops:block[1],offset:block[0]*btime,callback:false,gradients:block[2]})
		}
	}
	this.sequenceUpdate = function () {
		var obwidth = bwidth;
		dur = track.getDuration();
		bpm = track.get('tempo');
		bpb = track.get('bar');
		btime = bpb*60*1000/bpm;
		bwidth = timePos(btime);
		dfact = dur/btime;
		dwidth = bwidth*dfact;

		var $blocks = $track.find('.sequence-block');
		$blocks.each(function () {
			$(this).css({
				width:dwidth*$(this).find('.sequence-loop-input').val()
			});
			if (obwidth != bwidth) {
				$(this).css({
					left:Math.round($(this).position().left/obwidth)*bwidth
				});
			}
		});
		if ($blocks.length)
			this.updateSequence();

	}
	this.sequenceUpdate();
	this.updateSequence = function () {
		var $blocks = $track.find('.sequence-block'),
			sequence = [];
		$blocks.each(function () {
			var seq = [Math.round($(this).position().left/bwidth),
				$(this).find('.sequence-loop-input').val()*1];
			var gs = $(this).data("gradients");
			if (gs !== undefined)
				seq.push(gs);
			sequence.push(seq);
		});
		track.set('sequence',sequence);
		interface.storage.autoSave();
	}
	this.addBlock = function (pos) {
		var rep = track.get('loop'),
			roundPos;
		if (typeof pos == 'object') {
			roundPos = pos[0]*bwidth;
			rep = pos[1]*1;
		} else roundPos = Math.floor(pos/bwidth)*bwidth;
		if ((!rep) || rep < 1) rep = 1;
		var $block = $('<div>',{class:'sequence-block'}),
				$loop = $('<input>',{class:'sequence-loop-input',type:'number',min:1,max:99,step:1,value:rep});

		$block.css({
			left:roundPos,
			width:dwidth*rep
		})
		if (pos.length > 2)
			$block.data('gradients',pos[2]);

		$block
		.click(function() {
			selectedBlock = $(this);
			$track.find('.sequence-block.active').removeClass('active');
			$(this).addClass('active');
		})
		.resizable({
			handles:'e,w',
			start:function (ev,ui) {
				var $note = $(this);

				// Set values to keep relative positions later
				var l = $note.position().left,
					r = $note.position().left+$note.width();
				$note.attr('data-left',l)
				$note.attr('data-right',r)
			},
			resize:function (ev,ui) {
				// Round off widths to fit half notes on grid
				var $note = $(this),
					d = $note.data('ui-resizable').axis;
				// West direction, dragging the left edge
				if (d == "w") {
					var pr = $note.attr('data-right')*1,
						pl = ui.position.left,
						nl = Math.round(pl/bwidth)*bwidth;

					var pw = pr-nl,
						nloops = Math.round(pw/dwidth);
					if (nloops < 1) nloops = 1;
					var nw = nloops*dwidth;

					$note.find('.sequence-loop-input').val(nloops);

					// Alsoedo widthcauseo f loopy
					$note.css({
						left:nl,
						width:nw
					});
				}
				// East direction, dragging right edge
				else {
					var pw = ui.size.width,
						nloops = Math.round(pw/dwidth);
					if (nloops < 1) nloops = 1;
					var nw = nloops*dwidth;
					$note.find('.sequence-loop-input').val(nloops);
					$note.css({
						width:nw
					});
				}
				if (callbacks.resize)
					callbacks.resize($note);
			},
			stop:function () {
				_this.updateSequence(track);
			},
			containment:"parent"
		})
		.draggable({
			drag:function (ev,ui) {
				if ($block.is('.editing')) {
					return false;
				}
				var pl = ui.position.left,
					nl = Math.round(pl/bwidth)*bwidth;
				ui.position.left = nl;
			},
			stop:function (ev,ui) {
				if (!$block.is('.editing')) {
				// Make sure position is still round when letting go
				var pl = ui.position.left,
					nl = Math.round(pl/bwidth)*bwidth;
				ui.position.left = nl;
				_this.updateSequence(track);
				}
			},
			axis:'x',
			containment:"parent"
		})
		.dblclick(function (ev) {
			if (this != ev.target)
				return;
			$(this).remove();
			_this.updateSequence(track);
		});

		$loop.change(function () {
			$block.css({width:$(this).val()*1*dwidth});
			_this.updateSequence(track);
		})


			$block.append($loop);
		$track.append($block);
	}


	$track
	.click(function () {
		if (callbacks.activate)
			callbacks.activate(track);
	})
	.dblclick(function (ev) {
		if (this != ev.target)
			return;
		if (!dur) return;
		_this.addBlock(ev.offsetX);
		_this.updateSequence(track);

	})

	var sequence = track.get('sequence');
	for (var s in sequence)
		this.addBlock(sequence[s]);

	this.view = $track;
	this.model = track;
	if ($parent !== undefined)
		$parent.append($track);
}


function init(instruments) {
	var badList = ['bag_pipe','clavichord','lead_8_bass_lead','synth_voice','synthbrass_1','synthbrass_2','synthstrings_1','synthstrings_2'];
	for (var i=instruments.length-1;i>=0;i--) {
		if (badList.indexOf(instruments[i]) != -1)
			instruments.splice(i,1);
	}
	window.inst = instruments;
	// Removing loading screen
	$('#popup').remove();
	// Set available instruments loaded from names.json list
	window.instruments.available(instruments);
	// Initiate the main App
	window.interface = new Interface();
}

// Once the page & jQuery have loaded
$(function () {
	// Globally initialise instruments and notes
	// This will help load the default instrument
	window.instruments = new Instruments();
	window.notes = new Notes("qwerty",
	// window.notes = new Notes("`1234567890-=qwertyuiop[]asdfghjkl;'#\\zxcvbnm,./",
	'uk',true,4);
	// Load the MIDI plugin, then the MIDI instruments list from names.json
	// When all done this will run init above ^^^
	MIDI.loadPlugin({
		instruments:instruments.loaded(),
		onsuccess:function () {
			$.get('soundfont/names.json',init);
		}
	})
})



function Files(list,type,callback) {
	var cats = interface.storage.model.loadCats(type);
	// categories and search
	var $popup = $('<div>'),
			$actions = $('<div>',{class:'actions'}),
				$search = $('<input>',{placeholder:'Search'}),
				$add = $('<input>',{placeholder:'Add Cat.'}),
				$cats = $('<div>',{class:'dropdown hidden'});
			$list = $('<div>',{class:'content'});

	$popup
		.append($actions
			.append($search)
			.append($add)
			.append($cats))
		.append($list);

	function getCats(file) {
		var ret = [];
		for (var c in cats) {
			for (var C in cats[c]) {
				if (cats[c][C] == file)
					ret.push(c);
			}
		}
		return ret;
	}
	function hasCat(file) {
		for (var c in cats) {
			for (var C in cats[c]) {
				if (cats[c][C] == file)
					return true;
			}
		}
		return false;
	}
	function buildCats() {
		$cats.empty();
		for (var c in cats) {
			$cats.append($('<button>',{'data-cat':c}).html(c));
		}
	}
	function filter(name, sList, $el) {
		var c = 0;
		for (var s in sList)
			c += occurences(name, sList[s]);
		if (c)
			$el.removeClass('hidden');
		else $el.addClass('hidden');
		return c;
	}
	buildCats();

	for (var l in list) {
		var name = list[l];
		if (type == 'instrument')
			name = name.replace(/[\_]+/g,' ');
		$list.append(
			$('<button>',{
				class:'file '+type+(hasCat(list[l]) ? ' hascat':''),
				'data-file':list[l]})
				.html(name)
				.append($('<button>',{class:'add fa fa-plus'})
			));
	}
	$list.find('.file').click(function (ev) {
		if (this != ev.target) return;
		interface.popup();
		callback($(this).attr('data-file'));
	});
	$list.find('.file .add').click(function (ev) {
		if (this != ev.target) return;
		var $this = $(this);
		if ($this.is('.active')) return;
		var fileName = $this.closest('[data-file]').attr('data-file');
		var $addC = $('<input>',{placeholder:'Add Cat.'});
		$this.addClass('active').append($addC);
		$addC.keyup(function (ev) {
			var search = $addC.val();
			var $clone = $this.find('.dropdown');
			if (search) {
				var sList = search.split(' ').filter(String);
				for (var name in cats) {
					filter(name, sList, $clone.find("[data-cat='"+name.replace(/'/g,"\\'")+"']:not(.keep)"));
				}
				$clone.find('.active').removeClass('active');
				$clone.find(':not(.hidden):not(.keep)').eq(0).addClass('active');

				if (ev.key == 'Enter') {
					var $active = $clone.find('.active');
					if ($active.length) {
						cats[$active.attr('data-cat')].push(fileName);
						interface.storage.model.saveCats(type,cats);
						$active.addClass('keep');
						$addC.val('').keyup();
					} else {
						// cats[search] = [];
						// buildCats();
						// $add.val('');
					}
				}

			} else {
				$clone.find('.hidden').removeClass('hidden');
				$clone.find('.active').removeClass('active');
			}
		})
		.focus(function () {
			var $clone = $cats.clone();
			$this.append($clone)
			$clone.removeClass('hidden').css({
				left:$(this).position().left,
				top:$(this).position().top+$(this).outerHeight(),
				width:$(this).outerWidth()
			})	.find('.hidden').removeClass('hidden');
			$clone.find('.active').removeClass('active');
			var cats = getCats(fileName);
			for (var c in cats)
				$clone.find("[data-cat='"+cats[c].replace(/'/g,"\\'")+"']").addClass('keep');
		}).blur(function () {
			$(this).remove();
			$this.find('.dropdown').remove();
			$this.removeClass('active');
		}).focus();
	});
	interface.popup($popup);
	$search.keyup(function (ev) {
		var search = $search.val();
		if (search) {
			var sList = search.split(' ').filter(String);
			for (var l in list) {
				var name = list[l];
				filter(name,sList,$list.find(".file[data-file='"+name.replace(/'/g,"\\'")+"']"));
			}
			for (var name in cats) {

				var found = filter(name, sList, $cats.find("[data-cat='"+name.replace(/'/g,"\\'")+"']"));

				// Unhide categories found
				if (found) {
					for (var c in cats[name])
						$list.find(".file[data-file='"+cats[name][c].replace(/'/g,"\\'")+"']").removeClass('hidden');
				}
			}

			$cats.find('.active').removeClass('active');
			$cats.find(':not(.hidden)').eq(0).addClass('active');

			if (ev.key == 'Enter' && $cats.find('.active').length) {
				$cats.find('.active').click();
			}
		} else {
			$list.find('.file').removeClass('hidden');
			$cats.find('.hidden').removeClass('hidden');
			$cats.find('.active').removeClass('active');
		}
	});
	$add.keyup(function (ev) {
		var search = $add.val();
		if (search) {
			var sList = search.split(' ').filter(String);
			for (var name in cats) {
				filter(name, sList, $cats.find("[data-cat='"+name.replace(/'/g,"\\'")+"']"));
			}
			$cats.find('.active').removeClass('active');
			$cats.find(':not(.hidden)').eq(0).addClass('active');

			if (ev.key == 'Enter') {
				if ($cats.find('.active').length) {
					$add.val('');
				} else {
					cats[search] = [];
					interface.storage.model.saveCats(type,cats);
					buildCats();
					$add.val('');
				}
			}

		} else {
			$cats.find('.hidden').removeClass('hidden');
			$cats.find('.active').removeClass('active');
		}

	})
	$add.focus(function () {
		$cats.removeClass('hidden').css({
			left:$(this).position().left,
			top:$(this).position().top+$(this).outerHeight(),
			width:$(this).outerWidth()
		})	.find('.hidden').removeClass('hidden');
		$cats.find('.active').removeClass('active');
	}).blur(function () {
		$cats.addClass('hidden');
	});
	$search.focus(function () {
		$cats.removeClass('hidden').css({
			left:$(this).position().left,
			top:$(this).position().top+$(this).outerHeight(),
			width:$(this).outerWidth()
		})	.find('.hidden').removeClass('hidden');
		$cats.find('.active').removeClass('active');
		$cats.find('button').click(function () {
			$search.val($(this).attr('data-cat')).keydown();
			$(this).addClass('hidden');
		})
	}).blur(function () {
		$cats.addClass('hidden')
			.find('button').unbind('click');
	});




	function occurences(string, subString, allowOverlapping) {
		string += "";
		subString += "";
		string = string.toLowerCase();
		subString = subString.toLowerCase();
		if (subString.length <= 0) return string.length + 1;
		var n = 0,
			pos = 0,
			step = allowOverlapping ? 1 : subString.length;
		while (true) {
			pos = string.indexOf(subString, pos);
			if (pos >= 0) {
				++n;
				pos += step;
			} else break;
		}
		return n;
	}
}










function Grid (trackView, $parent) {

	var _this = this,
		noteH = 15,
		barH = 18,
		factor = 20,
		notesW = 40,
		minBarsN = 4,
		noteCount = notes.midiCount(),
		notesH = (noteH*noteCount),
		track = trackView.model,
		id = track.id(),
		bpm = track.get('tempo'),
		bpb = track.get('bar'),
		trackNotes = track.get('notes'),
		noteW,barW,barsN,barsW;


	var $editor = $('<div>',{id:'editor','data-track':id}),
			$toolBar = $('<div>',{id:'edit-tools'}),
				$tempoLabel = $('<label>',{}).html('BPM'),
					$tempo = $('<input>',{id:'tempo',value:bpm,type:'number',min:1,max:999}),
				$beatsLabel = $('<label>',{}).html('Beats'),
					$beats = $('<input>',{id:'beats',value:bpb,type:'number',min:2,max:5}),
				$barsLabel = $('<label>',{}).html('Bars'),
					$bars = $('<input>',{id:'bars',type:'number',min:4,max:99}),
				$save = $('<button>',{id:'edit-save', class:'fa fa-check'}).html(' Save'),
				$cancel = $('<button>',{id:'edit-cancel'}).html('cancel'),
			$container = $('<main>',{id:'midi-editor'}),
				$head = $('<header>',{id:'midi-bars','data-bpb':bpb}),
				$notes = $('<header>',{id:'midi-notes'}),
				$scroll = $('<div>',{id:'midi-scroll'}),
					$grid = $('<div>',{id:'midi-grid'}).css({height:notesH});
	$editor
		.append($toolBar
			.append($tempoLabel
				.append($tempo))
			.append($beatsLabel
				.append($beats))
			.append($barsLabel
				.append($bars))
			.append($save)
			// .append($cancel)
		)
		.append($container
			.append($notes)
			.append($head)
			.append($scroll
				.append($grid)))


	var callbacks = {
			close:false,
			save:false,
			scroll:false
		};
	this.callbacks = function (cbs) {
		for (var c in cbs)
			callbacks[c] = cbs[c];
	}

	function keyPos(key) {
		return ((noteCount-1)-key) * noteH;
	}
	function posKey(pos) {
		return (noteCount-1)-(Math.round((pos*2/noteH))/2);
	}
	function posTime(pos) {
		return  Math.round(pos * factor);
	}
	function timePos(time) {
		return Math.round(time / factor);
	}
	function setBPM(val) {
		if (val !== undefined)
			bpm = val;
		noteW = 60*1000/(bpm*factor);
		// Cascade update beats per bar
		setBPB();
	}
	function setBPB(val) {
		if (val !== undefined)
			bpb = val;
		barW = noteW*bpb;
		// Cascade update bars
		setBars();
	}
	function setBars(val) {
		if (val !== undefined) {
			barsN = val;
			// Find min bars by notes
			var NbarsN = 0;
			// ERR: Perhaps base this on currently displayed notes
			if (trackNotes.length)
				NbarsN = Math.ceil(bpm*(trackNotes[trackNotes.length-1][1]/1000)/(60*bpb));
			// Default to largest number of bars possible (out of that passed, enough to cover the notes, and the default minimum of 4)
			if (NbarsN > barsN)
				barsN = NbarsN;
			if (barsN < minBarsN)
				barsN = minBarsN;
		}
		barsW = barsN*barW;
		// Return the value, as it may have been limited and may no longer be what's passed
		return barsN;
	}
	// Set values initially to build their associated vars
	setBPM(bpm);
	$bars.val(setBars(0));

	function displayBars() {
		// Display bar columns

		$head.empty().attr('data-bpb', bpb);
		// Create bar list
		for (var b=0;b<barsN;b++)
			$head.append($('<div>',{class:'bar'}).html(b+1).css({width:barW,height:notesH+barH}));

		// Notes and grid maintain width
		$notes.find('.note').css({width:barsW+notesW})
		$grid.css('width', barsW);

	}

	function gridNoteActions($gridNotes) {
		$gridNotes
		.resizable({
			handles:'e,w',
			start:function (ev,ui) {
				var $note = $(this),
					d = $note.data('ui-resizable').axis;
				if (! $note.is('.active')) {
					$grid.find('.grid-note.active').removeClass('active');

					$note.addClass('active');
				}
				var min = 'falsy',max='falsy';
				// Set values to keep relative positions later
				$grid.find('.grid-note.active').each(function () {
					var $note = $(this),
						l = $note.position().left,
						r = $note.position().left+$note.width();
					$note.attr('data-left',l)
					$note.attr('data-right',r)
					if (min == 'falsy' || l < min)
						min = l;
					if (max == 'falsy' || r > max)
						max = r;
				}).attr({'data-min':min,'data-max':max});
			},
			resize:function (ev,ui) {
				// Round off widths to fit half notes on grid
				var $note = $(this),
					d = $note.data('ui-resizable').axis;
				// West direction, dragging the left edge
				if (d == "w") {
					var pr = $note.attr('data-right')*1,
						nl = util.ctrl ? ui.position.left :  Math.round((ui.position.left*2/noteW))*noteW/2;
					if ($note.is('.active')) {
						var min=$note.attr('data-min')*1,
							max=$note.attr('data-max')*1,
							fac = (max-nl)/(max-($note.attr('data-left')*1));
						$grid.find('.grid-note.active').each(function () {
							var $note = $(this),
								l = $note.attr('data-left')*1,
								r = $note.attr('data-right')*1,
								nl = max-((max-l)*fac),
								nr = max-((max-r)*fac);
							$note.css({
								left:nl,
								width:nr-nl
							});
						});
					} else {
						$note.css({
							left:nl,
							width:pr-nl
						});
					}
				}
				// East direction, dragging right edge
				else {
					var nw = util.ctrl ? ui.position.left+ui.size.width :  (Math.round((ui.position.left+ui.size.width)*2/noteW)*noteW/2);
					if ($note.is('.active')) {
						var min=$note.attr('data-min')*1,
							max=$note.attr('data-max')*1,
							fac = (nw-min)/(($note.attr('data-right')*1)-min);
						$grid.find('.grid-note.active').each(function () {
							var $note = $(this),
								l = $note.attr('data-left')*1,
								r = $note.attr('data-right')*1,
								nl = ((l-min)*fac)+min,
								nr = ((r-min)*fac)+min;
							$note.css({
								left:nl,
								width:nr-nl
							});
						});
					} else {
						$note.css({
							width:nw-ui.position.left
						});
					}
				}
			},
			stop:function () {
				// Remove attribues used
				$grid.find('.grid-note').removeAttr('data-right data-left data-min data-max');
				return;
			},
			containment:"parent"
		})
		.draggable({
			start:function (ev,ui) {
				var $note = $(this),
					handleNote = notes.midiToNote(posKey($note.position().top));
				if ($note.is('.active')) {
					// Get initial properties of selected notes so we can keep relative positions
					$grid.find('.grid-note.active').each(function () {
						var $note = $(this);
						$note.attr('data-left',$note.position().left);
						$note.attr('data-dist',
							notes.noteDistance(handleNote,
								notes.midiToNote(
									posKey($note.position().top)
								)));
					});
				} else {
					// Activate note if this note is not active
					$grid.find('.grid-note.active').removeClass('active');
					$note.addClass('active');
				}
			},
			drag:function (ev,ui) {
				var $note = $(this),
					pl = ui.position.left,
					pt = ui.position.top,
					nl = util.ctrl ? pl : Math.round((pl*2/noteW))*noteW/2,
					nt = posKey(pt);
				// Test if flat note exists
				if (nt%1 && ! notes.midiToNote(nt))
					ui.position.top = $note.position().top;
				else ui.position.top = keyPos(nt);
				ui.position.left = nl;

				// Positions to be relative to
				var ldelt = ui.position.left-($note.attr('data-left')*1),
					handleNote = notes.midiToNote(posKey(ui.position.top));

				// Set relative positions of active notes
				$grid.find('.grid-note.active').each(function () {
					var $note = $(this);

					$note.css({
						top:keyPos(
								notes.noteToMidi(
									notes.distanceNote(
										handleNote, $note.attr('data-dist')*1))),
						left:($note.attr('data-left')*1)+ldelt,
					})
				})
			},
			stop:function (ev,ui) {
				// Remove attributes used
				$grid.find('.grid-note').removeAttr("data-left data-dist");
			},
			containment:"parent"
		})
		.click(function () {
			if ($(this).is('.active')) {
				if (util.shift)
					$(this).removeClass('active');
 				return;
			}
			if (! util.shift)
				$grid.find('.grid-note.active').removeClass('active');
			$(this).addClass('active');
		})
		// Double click to remove note
		.dblclick(function () {
			$(this).remove();
		});
	}
	function displayGrid () {
		// Display recorded notes

		var minNote = 1000,
			maxNote = 0;
		$grid.empty();
		// Build note elements from on/off list
		var notesActive = notes.falseNotes();
		for (var n in trackNotes) {
			var note = trackNotes[n],
				mNote = MIDI.keyToNote[note[2]];
			if (mNote > maxNote)
				maxNote = mNote;
			if (mNote < minNote)
				minNote = mNote;
			if (note[0]) {
				notesActive[note[2]] = note[1];
			} else {
				if (notesActive[note[2]]) {
					$grid.append($('<div>',{class:'grid-note'}).css({
						width:(note[1] - notesActive[note[2]])/factor,
						left:notesActive[note[2]]/factor,
						top:keyPos(notes.noteToMidi(note[2]))
					}))
					notesActive[note[2]] = false;
				}
			}
		}
		$scroll.scrollTop(
			((noteH*
				(notes.midiCount()-((notes.noteToMidi(MIDI.noteToKey[minNote])+
				notes.noteToMidi(MIDI.noteToKey[maxNote]))/2))
			)-($scroll.height()/2)));

		gridNoteActions($grid.find('.grid-note'));

		// Grid events (unbind first)
		$grid.unbind('dblclick mousedown')
		// Double click to add new note
		.dblclick(function (ev) {
			if (this != ev.target)
				return;
			var pl = ev.offsetX,
				pt = ev.offsetY,
				nl = Math.floor(pl/noteW),
				nt = Math.floor(pt/noteH);
			if (nt%1 && ! notes.midiToNote(noteCount-(1+nt)))
				nt = Math.ceil(nt);
			var $newNote = $('<div>',{class:'grid-note'})
				.css({
					left:nl*noteW,
					top:nt*noteH,
					width:noteW,
					height:noteH
				});
			$grid.append($newNote);
			gridNoteActions($newNote);
		})
		// Multitple element selection
		$container.mousedown(function (ev) {
			if ($(ev.target).is('.grid-note,.ui-resizable-handle'))
				return;
			var pos = {left:ev.pageX,top:ev.pageY},
				$select = $('<div>',{id:'grid-select'}).css(pos);

			var curActive = $grid.find('.grid-note.active');
			if (! util.shift)
				curActive.removeClass('active');
			$grid.append($select);
			var move = function (ev) {
				var pos1 = $.extend({},pos),
					pos2 = {left:ev.pageX,top:ev.pageY};
				if (pos1.left > pos2.left) {
					var p = pos1.left;
					pos1.left = pos2.left;
					pos2.left = p;
				}
				if (pos1.top > pos2.top) {
					var p = pos1.top;
					pos1.top = pos2.top;
					pos2.top = p;
				}
				$select.css(pos1).css({width:pos2.left-pos1.left,height:pos2.top-pos1.top})

				var rpos = {
					left:$grid.offset().left,
					top:$grid.offset().top,
				}

				$grid.find('.grid-note').each(function () {
					var $note = $(this);
					if ($note.position().left+rpos.left > pos1.left &&
						$note.position().left+rpos.left+$note.width() < pos2.left &&
						$note.position().top+rpos.top > pos1.top &&
						$note.position().top+rpos.top+$note.height() < pos2.top) {
						if (util.shift && curActive.index($note) != -1) {
							$note.removeClass('active');
							$note.addClass('deactive');
						} else $note.addClass('active');


					} else $note.removeClass('active');
				})
				if (util.shift) {
					curActive.addClass('active')
					$grid.find('.grid-note.deactive').removeClass('active deactive');
				}
			}
			$container.mousemove(move)
			$container.mouseup(function () {
				$select.remove();
				$container.unbind('mouseup')
					.unbind('mousemove', move);
			})
		})
	}
	function displayNotes() {
		// Display note rows (white notes only)

		$notes.empty();
		// Create note list
		notes.each('midi', function (k, note) {
			$notes.prepend(
				$('<div>',{class:'note'+(note[1] == 'b' ? ' flat' : '')})
					.html(note));
		});
	}

	displayNotes();
	displayBars();

	this.notes = function () {

		var trackNotes = [];

		// Read in physical notes as starts and stops
		$grid.find('.grid-note').each(function () {
			var $note = $(this),
				pl = $note.position().left,
				nl = posTime(pl),
				sl = posTime(pl+$note.outerWidth()),
				nt = posKey($note.position().top),
				nk = notes.midiToNote(nt);

			// Invalid? Assume white key
			if (! nk)
				nk = notes.midiToNote(Math.ceil(nt));

			// Having a time of 0 can be buggy
			if (nl < 1)
				nl = 1;
			if (sl < 1)
				sl = 1;

			// Add note start and end
			trackNotes.push([true,nl,nk]);
			trackNotes.push([false,sl,nk]);
		});

		// Sort notes with smallest time first
		trackNotes.sort(function (a,b) {
			// ERR: Make sure stops of the same time as starts go before the starts
			return a[1]-b[1];
		});
		return trackNotes;
	}
	this.close = function () {
		// Close the editor
		$editor.remove();
		$('body').unbind('keydown',keyDown);
		$('body').unbind('keyup',keyUp);
		if (callbacks.close)
			callbacks.close();
	}
	this.save = function () {
		// Save the edited notes
		var trackNotes = this.notes();

		// Update track properties
		track.set('notes',trackNotes);
		track.set('tempo',bpm);
		track.set('bars',bpb);

		if (callbacks.save)
			callbacks.save();
	}

	// Close without save
	$cancel.click(function () {
		_this.close();
	});
	// Close with save
	$save.click(function () {
		_this.save();
		_this.close();
	});
	// Update toolbar options
	$tempo.change(function () {
		setBPM($(this).val()*1);
		displayBars();
	});
	$bars.change(function () {
		var val = setBars($(this).val()*1);
		$(this).val(val);
		displayBars();
	});
	$beats.change(function () {
		setBPB($(this).val()*1);
		displayBars();
	});
	// Simualte scrolling for fixed elements (constant headers)
	$scroll.scroll(function () {
		$head.css('left', -$(this).scrollLeft());
		$notes.css('top', -$(this).scrollTop());
		if (callbacks.scroll)
			callbacks.scroll($(this).scrollLeft(),$(this).scrollTop());
	});

	this.view = $editor;
	this.model = trackView;
	if ($parent === undefined)
		$parent = $('body');
	$parent.append($editor);
	displayGrid();

	var util = {
		ctrl:false,
		shift:false,
		copying:false,
		cutting:false,
		history:[],
		copy:function ($els) {
			if (! $els.length)
				return;

			this.copying = $els.clone();


			var nl = Math.floor(this.mouse.x/noteW)*noteW,
				nt = Math.floor(this.mouse.y/noteH)*noteH;

			var handleNote = notes.midiToNote(posKey(nt));

			this.copying.each(function (i) {
				var $note = $(this),
					$onote = $els.eq(i);
				$note.attr('data-left',nl-$onote.position().left);
				$note.attr('data-dist',
					notes.noteDistance(handleNote,
						notes.midiToNote(
							posKey($onote.position().top)
						)));
			});
			this.cutting = false;
		},
		cut:function ($els) {
			if (! $els.length)
				return;

			var nl = Math.floor(this.mouse.x/noteW)*noteW,
				nt = Math.floor(this.mouse.y/noteH)*noteH;

			var handleNote = notes.midiToNote(posKey(nt));
			$els.each(function (i) {
				var $note = $(this);
				$note.attr('data-left',nl-$note.position().left);
				$note.attr('data-dist',
					notes.noteDistance(handleNote,
						notes.midiToNote(
							posKey($note.position().top)
						)));
			});
			this.cutting = $els.detach();
			this.copying = false;
		},
		paste:function (pos) {
			if (this.copying || this.cutting) {
				var c = this.copying ? this.copying : this.cutting,
					copy = c
						.clone()
						.addClass('active');
				$grid.find('.grid-note.active').removeClass('active');



				var nl = Math.floor(this.mouse.x/noteW)*noteW,
					nt = Math.floor(this.mouse.y/noteH)*noteH;


				// Positions to be relative to
				var handleNote = notes.midiToNote(posKey(nt));

				// Set relative positions of active notes
				copy.each(function () {
					var $note = $(this);
					$note.css({
						top:keyPos(
								notes.noteToMidi(
									notes.distanceNote(
										handleNote, $note.attr('data-dist')*1))),
						left:nl-($note.attr('data-left')*1),
					})
				})

				$grid.append(copy);
				gridNoteActions(copy);
			}
		},
		undo:function () {

		},
		redo:function () {

		},
		// position:{left:0,},
		mouse:{x:0,y:0}
	}
	$grid.mousemove(function (ev) {
		if (this != ev.target)
			return;
		util.mouse.x = ev.offsetX;
		util.mouse.y = ev.offsetY;
	})

	function keyDown (ev) {
		if (ev.ctrlKey) util.ctrl = true;
		if (ev.shiftKey) util.shift = true;
		var key = getKey(ev);
		if (! ev.ctrlKey)
			return;
		if (key > 90)
			key -= 32;
		// Ctrl+C
		if (key == 67) {
			util.copy($grid.find('.grid-note.active'));
		}
		// Ctrl+V
		else if (key == 86) {
			util.paste();
		}
		// Ctrl+X
		else if (key == 88) {
			util.cut($grid.find('.grid-note.active'));
		}
		// Ctrl+Z
		else if (key == 90) {
			util.undo();
		}
		// Ctrl+Y
		else if (key == 89) {
			util.redo();
		}
	}
	function keyUp (ev) {
		util.ctrl = ev.ctrlKey;
		util.shift = ev.shiftKey;
	}
	$('body').keydown(keyDown);
	$('body').keyup(keyUp);
}


// Bezier Gradient for volume ect.
var gradDefs = {
	pointR:5,
	pathR:10,
	maxRuns:100,
	approxTolerance:0.1,
	width:600,
	height:200,
	invertY:true,
	padding:20,
	proportional:true
};
var gradProps = ['volume','offset','pitch'];

function Paths(config,paths) {
	this.paths = [];
	if (paths === undefined || (! paths instanceof Array) || paths.length == 0)
		paths = [{s:{x:0,y:0},e:{x:config.proportional ? 1 : config.width,y:0}}];
	for (var p in paths) {
		var first = p == 0,
			last = p == paths.length-1,
			prev = this.paths[this.paths.length-1],
			cpath = paths[p],
			path = {};
		for (var c in cpath) {
			var P = {};
			P.x = cpath[c].x;
			P.y = (config.invertY ? (config.proportional ? 1 : config.height)-cpath[c].y : cpath[c].y);
			path[c] = P;
		}
		if (first)
			var start = new Point(config,path.s,'start');
		else start = prev.end;
		var end = new Point(config,path.e,last ? 'end' : 'join');
		var bez = {};
		if ('a' in path)
			bez.a = new Point(config,path.a,'hand');
		if ('b' in path)
			bez.b = new Point(config,path.b,'hand');
		this.paths.push(new Path(config,start,end,bez));
	}
	var selected = this.paths[0];

	this.removePoint = function (point) {
		if (point.fixed) return;
		var start = false,
			end = false;
		for (var p in this.paths) {
			var rem = this.paths[p].removePoint(point);
			if (rem && rem != 'bezier') {
				if (rem == 'start')
					end = this.paths[p];
				else if (rem == 'end')
					start = this.paths[p];
			} else if (rem == 'bezier') break;
		}
		if (start && end) {
			var sbz = start.bezier(),
				ebz = end.bezier();
			if (!(sbz == 0 &&
				ebz == 0)) {
				function Default() {
					var max = Math.max(sbz,ebz);
					var bez = {};
					if (max >= 1) {
						if (sbz >= 1)
							bez.a = start.bez.a;
						else bez.a = end.bez.a;
					}
					if (max == 2) {
						if (ebz == 2)
							bez.b = end.bez.b;
						else bez.b = start.bez.b;
					}
					start.bezier(bez,true);
				}
				if (sbz != ebz ||
					(sbz == 1 && ebz == 1))
					Default();
				else {
				// if (point.hasMoved()) {
				// 	// ERR:not moved, if start.bez.b and end.bez.a not approx linear
				// } else {
					var R = config.pathR;
					if (config.proportional)
						R /= config.height;
					var run = false;
					if (sbz == 2) {
						if (point.x<start.bez.b.x && start.bez.b.x-point.x>R)
							Default();
						else if (point.x>end.bez.a.x && point.x-end.bez.a.x>R)
							Default();
						else {
							var xp = (point.x-start.bez.b.x)/(end.bez.a.x-start.bez.b.x);
							var y2 = start.bez.b.y+((end.bez.a.y-start.bez.b.y)*xp);
							if (Math.abs(y2-point.y) > R)
								Default();
							else {
								var mb = mergeBezier(start.start,
									start.bez.a,start.bez.b,
									point,
									end.bez.a,end.bez.b,
									end.end);
								run = true;
							}
						}
					} else if (sbz == 1) {
						var mb = mergeBezierQ(start.start,
							start.bez.a,
							point,
							end.bez.a,
							end.end);
						run = true;
					}
					if (run) {
						for (var m in mb) {
							for (var n in mb[m]) {
								if (isNaN(mb[m][n]))
									run = false;
							}
						}
						if (run) {
							console.log(mb)
							start.bez.a.pos(mb[0]);
							if (sbz == 2)
								start.bez.b.pos(mb[1]);
						}
						// else start.bezier(new Point('#000',mb[1]));
					}
				}
			}
			start.end = end.end;
			var sel = (selected == end);
			this.paths.splice(this.paths.indexOf(end),1);
			if (sel)
				selected = start;
		}
		// points.splice(points.indexOf(point),1);
		config.canvas.redraw();
	}
	this.select = function(path) {
		selected = path;
		config.canvas.redraw();
	}

	this.addPoint = function (pos, type) {
		selected.bezier(new Point(config,pos, type));
		config.canvas.redraw();
	}
	this.getHandles = function (point) {
		var ret = [];
		for (var p in this.paths) {
			var path = this.paths[p];
			if (path.start == point) {
				if (path.bezier() > 0)
					ret.push(path.bez.a);
			} else if (path.end == point) {
				if (path.bezier() > 1)
					ret.push(path.bez.b);
			}
		}
		return ret;
	}

	this.hitPoint = function (x,y) {
		for (var p in this.paths) {
			var path = this.paths[p].hitPoint(x,y);
			if (path) return path;
			// var point = points[p],
			// 	r = Math.sqrt(
			// 		Math.pow(x-point.x,2)+
			// 		Math.pow(y-point.y,2));
			// if (r < R) {
			// 	return point;
			// }
		}
		return false;
	}
	this.hitPath = function (x,y) {
		for (var p in this.paths) {
			var path = this.paths[p].hitPath(x,y);
			if (path) return this.paths[p];
		}
		return false;
	}
	this.splitPath = function(path, x, y) {
		var bz = path.bezier();
		if (bz == 0) {
			var n = new Point(config,{x:x,y:y},'join');
			var i = this.paths.indexOf(path);
			var e = path.end;
			path.end = n;
			this.paths.splice(i+1,0,new Path(config,n,e));
		} else {
			var px = path.getAtX(x);
			if (bz == 2) {
				var sl = sliceBezier(path.start,
							path.bez.a,
							bz == 2 ? path.bez.b : path.end,
							path.end,px.t);
			} else {
				var sl = sliceBezierQ(path.start,
							path.bez.a,
							path.end,px.t);
			}

			var n = new Point(config,{x:x,y:px.y},'join');
			var i = this.paths.indexOf(path);
			var e = path.end;
			path.end = n;

			path.bez.a.pos(sl[0].a);
			if (bz == 2)
				path.bez.b.pos(sl[0].b);
			var objs = {a:new Point(config,sl[1].a,'hand')};
			if (bz == 2)
				objs.b = new Point(config,sl[1].b,'hand');

			this.paths.splice(i+1,0,
				new Path(config,n,e,objs));

		}
		config.canvas.redraw();
	}
	function mergeBezierQ(start,sa,common,ea,end) {
		var v1 = start,
			sax = sa.x,
			say = sa.y,
			eax = ea.x,
			eay = ea.y;

		var c;
		if (end.x == -v1.x) {
			c = (sax-v1.x)/(eax+sax-(2*v1.x));
		} else {
			var ts = Math.sqrt(Math.abs((-4*end.x*sax)+(4*end.x*v1.x)+(eax*eax)+(2*eax*sax)-(4*eax*v1.x)+(sax*sax)-(8*sax*v1.x)+(8*v1.x*v1.x)));
			var c1 = (ts+eax+sax-(2*v1.x))/(2*(end.x+v1.x)),
				c2 = (-ts+eax+sax-(2*v1.x))/(2*(end.x+v1.x));
			// console.log(end.x+v1.x,c1,c2,ts,(-4*end.x*sax)+(4*end.x*v1.x)+(eax*eax)+(2*eax*sax)-(4*eax*v1.x)+(sax*sax)-(8*sax*v1.x)+(8*v1.x*v1.x))
			c = c1;
			if (c < 0 || c > 1)
				c = c2;
		}
		// console.log(c)
		// var t1 = (v1.x-sax)/(v1.x-v2.x),
		// 	t2 = (eax-v2.x)/(end.x-v2.x);
		// console.log(t1,t2,t3)
		// var c = t1;
		// if (c < 0 || c > 1)
		// 	c = t2;
		// if (c < 0 || c > 1)
		// 	c = t3;
		// c = Math.max(0, Math.min(1, position));
		// var sax = (v1.x + (v2.x - v1.x) * c),
		// 	say = (v1.y + (v2.y - v1.y) * c),
		// 	eax = (v2.x + (end.x - v2.x) * c),
		// 	eay = (v2.y + (end.y - v2.y) * c);
		var v2 = {
			x:((sax-v1.x)/c)+v1.x,
			y:((say-v1.y)/c)+v1.y,
		}

		return [v2];
		// return [{a:{x:sax,y:say}},{a:{x:eax,y:eay}}];
	}
	function mergeBezier(start,
			starta,startb,
			common,
			enda,endb,
			end) {
		// x1,y1,
		// x12,y12,x123,y123,
		// x1234,y1234,
		// x234,y234,x34,y34,
		// x4,y4,
		var x1 = start.x,
			y1 = start.y,
			x12 = starta.x,
			y12 = starta.y,
			x123 = startb.x,
			y123 = startb.y,
			x1234 = common.x,
			y1234 = common.y,
			x234 = enda.x,
			y234 = enda.y,
			x34 = endb.x,
			y34 = endb.y,
			x4 = end.x,
			y4 = end.y;

		var xt = (x1234-x123)/(x234-x123),
			yt = (y1234-y123)/(y234-y123);
		t = xt;
		if (t < 0 || t > 1) t = yt;

		var x2 = (x12-x1)/t+x1,
			y2 = (y12-y1)/t+y1,
			x3 = (x4-(x34/t))/(1-(1/t)),
			y3 = (y4-(y34/t))/(1-(1/t));

		return [{x:x2,y:y2},{x:x3,y:y3}];
	}
	function sliceBezierQ(start,a,end,position) {
		//stackoverflow.com/questions/37082744/split-one-quadratic-bezier-curve-into-two
		var v1 = start,
			v2 = a;
		c = Math.max(0, Math.min(1, position));
		var sax = (v1.x + (v2.x - v1.x) * c),
			say = (v1.y + (v2.y - v1.y) * c),
			eax = (v2.x + (end.x - v2.x) * c),
			eay = (v2.y + (end.y - v2.y) * c);
		return [{a:{x:sax,y:say}},{a:{x:eax,y:eay}}];
	}
	function sliceBezier(start,a,b,end,t) {
		// x1,y1, x2,y2, x3,y3, x4,y4,
		var x1 = start.x,
			y1 = start.y,
			x2 = a.x,
			y2 = a.y,
			x3 = b.x,
			y3 = b.y,
			x4 = end.x,
			y4 = end.y;

		var x12 = (x2-x1)*t+x1,
			y12 = (y2-y1)*t+y1,
			x23 = (x3-x2)*t+x2,
			y23 = (y3-y2)*t+y2,
			x34 = (x4-x3)*t+x3,
			y34 = (y4-y3)*t+y3,

			x123 = (x23-x12)*t+x12,
			y123 = (y23-y12)*t+y12,
			x234 = (x34-x23)*t+x23,
			y234 = (y34-y23)*t+y23;

		return [{
			a:{x:x12,y:y12},
			b:{x:x123,y:y123},
		},{
			a:{x:x234,y:y234},
			b:{x:x34,y:y34},
		}];
	}

	this.draw = function (drawPoints, lineStyle) {
		for (var p in this.paths) {
			var path = this.paths[p];
			path.draw(path == selected, drawPoints, lineStyle);
		}
	}
	this.getPaths = function () {
		var ret = [];
		for (var p in this.paths) {
			var path = this.paths[p];
			var first = p == 0,
				last = p == this.paths.length-1;
			var Npath = {};
			if (first)
				Npath.s = path.start.pos();
			Npath.e = path.end.pos();

			for (var b in path.bez)
				Npath[b] = path.bez[b].pos();

			for (var n in Npath) {
				if (config.invertY)
					Npath[n].y = (config.proportional ? 1 : config.height)-Npath[n].y;
			}
			ret.push(Npath);
		}
		return ret;
	}
	this.getAtX = function (x) {
		for (var p in this.paths) {
			var path = this.paths[p];
			if (path.start.x <= x && path.end.x >= x) {
				var P = path.getAtX(x);
				if (P) {
					if (config.invertY)
						P.y = (config.proportional ? 1 : config.height)-P.y;
					return P;
				}
			}
		}
		return false;
	}
}
function Path(config,start,end,bez) {
	this.start = start;
	this.end = end;
	if (bez === undefined) bez = {};
	this.bez = bez;
	var bz = Object.keys(bez).length;
	var handSwap = false;



	var quadC = function (t) {
		return {
			x:(((this.start.x-(2*this.bez.a.x)+this.end.x)*t+(2*(this.bez.a.x-this.start.x)))*t)+this.start.x,
			y:(((this.start.y-(2*this.bez.a.y)+this.end.y)*t+(2*(this.bez.a.y-this.start.y)))*t)+this.start.y,
		}
	};
	this.quadT = function (x) {
		var x0 = this.start.x,
			x1 = this.bez.a.x,
			x2 = this.end.x;
		if (x2+x0 == (2*x1)) {
			return (x0-x)/(2*(x0-x1));
		} else {
			var a = Math.sqrt((x2*x)-(x2*x0)+(x*x0)-(2*x*x1)+(x1*x1)),
				b = (x2+x0-(2*x1));
			var t1 = (a-x0+x1)/-b,
				t2 = (a+x0-x1)/b;
			var t = t1;
			if (t < 0 || t > 1)
				t = t2;
		}
		return t;
	};
	this.quadCx = function (x) {
		var t = this.quadT(x);
		var y0 = this.start.y,
			y1 = this.bez.a.y,
			y2 = this.end.y;
		return {
			x:x,
			y:(((y0-(2*y1)+y2)*t+(2*(y1-y0)))*t)+y0,
			t:t
		}
	};
	var brs = 0;
	this.beziCxB = function (n) {

		var x0 = this.start.x,
			x1 = this.bez.a.x,
			x2 = this.bez.b.x,
			x3 = this.end.x,
			y0 = this.start.y,
			y1 = this.bez.a.y,
			y2 = this.bez.b.y,
			y3 = this.end.y;
		var ret = [],lost=[];
		ret.push({x:x0,y:y0});
		tl = 0;
		for (var t=1;t<n;t++) {
			var pf = this.beziCF(x0+((x3-x0)*t/n),tl);
			if (pf) {
				tl = pf.t;
				ret.push(pf.p);
			} else lost.push(t);
		}
		for (var l in lost) {
			var lo = lost[l];
			ret.splice(lo,0,{
				x:(ret[lo-1].x+ret[lo].x)/2,
				y:(ret[lo-1].y+ret[lo].y)/2,
			});
		}
		ret.push({x:x3,y:y3});
		return ret;
	};
	this.beziCF = function (x,te,r) {
		var x0 = this.start.x,
			x1 = this.bez.a.x,
			x2 = this.bez.b.x,
			x3 = this.end.x,
			y0 = this.start.y,
			y1 = this.bez.a.y,
			y2 = this.bez.b.y,
			y3 = this.end.y;
		if (r === undefined) r = 0;
		r += 1;
		if (r > config.maxRuns) {
			return false;
		}

		var at = config.approxTolerance;
		if (config.proportional)
			at /= config.width;

		var b = this.beziC(te);
		if (Math.abs(b.x-x) > at) {
			var p = (Math.abs((b.x-x)/(x0-x3)))/2;
			if (b.x < x) {
				return this.beziCF(x,te+p,r);
			} else {
				return this.beziCF(x,te-p,r);
			}
		} else return {t:te,p:{x:x,y:b.y},r:r};
	};
	this.beziCx = function (x,te) {

		var x0 = this.start.x,
			x1 = this.bez.a.x,
			x2 = this.bez.b.x,
			x3 = this.end.x,
			y0 = this.start.y,
			y1 = this.bez.a.y,
			y2 = this.bez.b.y,
			y3 = this.end.y;

		if (te === undefined) {
			brs = 0;
			te = 0.5;
		}
		if (te < 0) te = 0;
		if (te > 1) te = 1;
		brs += 1;
		if (brs > config.maxRuns) return false;
		var b = this.beziC(te);

		var at = config.approxTolerance;
		if (config.proportional)
			at /= config.width;

		if (Math.abs(b.x-x) > at) {
			var p = (Math.abs((b.x-x)/(x0-x3)))/2;
			var b1 = this.beziC(te-p);
			var b2 = this.beziC(te+p);
			if (x < b.x) {
				if (b1.x < b.x)
					return this.beziCx(x,te-p);
				else return this.beziCx(x,te+p);
			} else {
				if (b1.x > b.x)
					return this.beziCx(x,te-p);
				else return this.beziCx(x,te+p);
			}
		} else return {x:x,y:b.y,t:te};
	};
	this.beziC = function (t) {
		var x0 = this.start.x,
			x1 = this.bez.a.x,
			x2 = this.bez.b.x,
			x3 = this.end.x,
			y0 = this.start.y,
			y1 = this.bez.a.y,
			y2 = this.bez.b.y,
			y3 = this.end.y;

		var T = (1-t);
		return {
			x:(T*T*T*x0)+(3*t*T*T*x1)+(3*t*t*T*x2)+(t*t*t*x3),
			y:(T*T*T*y0)+(3*t*T*T*y1)+(3*t*t*T*y2)+(t*t*t*y3),
		}
	};

	this.removePoint = function (point) {
		if (point == this.start)
			return "start";
		if (point == this.end)
			return "end";
		if (point == this.bez.b) {
			delete this.bez.b;
			bz -= 1;
			return "bezier";
		}
		if (point == this.bez.a) {
			if (bz == 2) {
				this.bez.a = this.bez.b;
				delete this.bez.b;
				handSwap = (!handSwap);
			} else delete this.bez.a;
			bz -= 1;
			return "bezier";
		}
		return false;
	}
	this.getAtX = function (x) {
		if (bz == 0) {
			var xp = (x-this.start.x)/(this.end.x-this.start.x);
			var y2 = this.start.y+((this.end.y-this.start.y)*xp);
			return {y:y2,t:xp,x:x};
		} else
		if (bz == 1) {
			return this.quadCx(x);
		} else if (bz == 2) {
			return this.beziCx(x);
		}
		return false;
	}
	function prop (p) {
		if (config.proportional) {
			if (p=='x')
				return config.width;
			else if (p=='y')
				return config.height;
		} else return 1;
	}
	this.draw = function (selected, drawPoints, lineStyle) {
		if (drawPoints === undefined) drawPoints = true;
		config.canvas.ctx.beginPath();
		config.canvas.ctx.moveTo(this.start.x*prop('x'),this.start.y*prop('y'));
		if (bz == 0) {
			config.canvas.ctx.lineTo(this.end.x*prop('x'),this.end.y*prop('y'));
		} else if (bz == 1) {
			config.canvas.ctx.quadraticCurveTo(
				this.bez.a.x*prop('x'),this.bez.a.y*prop('y'),
				this.end.x*prop('x'),this.end.y*prop('y'));
		} else if (bz == 2) {
			config.canvas.ctx.bezierCurveTo(
				this.bez.a.x*prop('x'),this.bez.a.y*prop('y'),
				this.bez.b.x*prop('x'),this.bez.b.y*prop('y'),
				this.end.x*prop('x'),this.end.y*prop('y'));
		}
		config.canvas.ctx.lineWidth = 2;
		if (lineStyle === undefined)
			config.canvas.ctx.strokeStyle = selected ? '#88f' : '#000';
		else config.canvas.ctx.strokeStyle = lineStyle;
		config.canvas.ctx.stroke();

		if (! drawPoints)
			return;
		/*
		config.canvas.ctx.beginPath();
		var s = Math.ceil((this.end.x-this.start.x)/100);
		for (var t=0;t<=s;t++) {
			var x = this.start.x+((this.end.x-this.start.x)*t/s),
				p = this.getAtX(x);
			if (p) {
				if (t == 0)
					config.canvas.ctx.moveTo(x,p.y+40);
				else config.canvas.ctx.lineTo(x,p.y+40);
			}
		}
		config.canvas.ctx.strokeStyle = "#000";
		config.canvas.ctx.stroke();
		*/

		if (bz>0) {
			config.canvas.ctx.beginPath();
			var x = handSwap ? this.end.x : this.start.x,
				y = handSwap ? this.end.y : this.start.y;
			config.canvas.ctx.moveTo(x*prop('x'),y*prop('y'));
			config.canvas.ctx.lineTo(this.bez.a.x*prop('x'),this.bez.a.y*prop('y'));
			config.canvas.ctx.lineWidth = 1;
			config.canvas.ctx.stroke();
		}
		if (bz>1) {
			config.canvas.ctx.beginPath();
			var x = handSwap ? this.start.x : this.end.x,
				y = handSwap ? this.start.y : this.end.y;
			config.canvas.ctx.moveTo(x*prop('x'),y*prop('y'));
			config.canvas.ctx.lineTo(this.bez.b.x*prop('x'),this.bez.b.y*prop('y'));
			config.canvas.ctx.lineWidth = 1;
			config.canvas.ctx.stroke();
		}
		this.start.draw();
		this.end.draw();
		for (var b in this.bez)
			this.bez[b].draw();
	}
	this.bezier = function (bez,set) {
		if (bez === undefined) return bz;
		if (set === undefined) set = false;
		if (set)
			this.bez = bez
		else {
			if (bz == 0)
				this.bez.a = bez;
			else if (bz == 1) {
				if (handSwap) {
					this.bez.b = this.bez.a;
					this.bez.a = bez;
					handSwap = false;
				} else this.bez.b = bez;
			} else return false;
		}
		bz = Object.keys(this.bez).length;
		return true;
	}
	this.hitPoint = function (x,y) {
		x -= config.padding;
		y -= config.padding;
		var points = [this.start,this.end];
		for (var b in this.bez)
			points.push(this.bez[b]);
		for (var p in points) {
			var point = points[p],
				px = point.x*prop('x'),
				py = point.y*prop('y');
			var r = Math.sqrt(
					Math.pow(x-px,2)+
					Math.pow(y-py,2));
			if (r < config.pointR) {
				return point;
			}
		}
	}

	this.hitPath = function (x,y) {
		x -= config.padding;
		y -= config.padding;
		x /= prop('x');
		y /= prop('y');
		var R = config.pathR;
		R /= prop('y');
		if (bz == 0) {
			if (x<this.start.x && this.start.x-x>R) return false;
				if (x>this.end.x && x-this.end.x>R) return false;
			var xp = (x-this.start.x)/(this.end.x-this.start.x);
			var y2 = this.start.y+((this.end.y-this.start.y)*xp);
			if (Math.abs(y2-y) < R)
				return true;

		} else
		if (bz == 1) {
			var s = Math.ceil((this.end.x-this.start.x)/R);
			for (var t=0;t<=s;t++) {
				var p = this.quadCx(this.start.x+((this.end.x-this.start.x)*t/s));
				var dx = x-p.x,
					dy = y-p.y,
					r = Math.sqrt((dx*dx)+(dy*dy));
				if (r < R)
					return true;
			}
		} else
		if (bz == 2) {
			var s = Math.ceil((this.end.x-this.start.x)/R);
			for (var t=0;t<=s;t++) {
				var p = this.beziCx(this.start.x+((this.end.x-this.start.x)*t/s));
				if (p) {
					var dx = x-p.x,
						dy = y-p.y,
						r = Math.sqrt((dx*dx)+(dy*dy));
					if (r < R)
						return true;
				}
			}
		}
		return false;
		// config.canvas.ctx.lineWidth = 20;
		// drawPaths();
		// return (config.canvas.ctx.isPointInPath(x,y))
	}
}
function Point (config,pos,type) {
	this.size = 5;
	switch(type) {
		case 'start':
			this.colour = "#0f0";
			break;
		case 'end':
			this.colour = "#f00";
			break;
		case 'join':
			this.colour = "#00f";
			break;
		case 'hand':
			this.colour = "#000";
			this.size = 2;
			break;
	}
	if (type == 'start' || type == 'end')
		this.fixed = true;
	else this.fixed = false;

	this.outOfBounds = function(pos) {
		var ret = {x:pos.x,y:pos.y},
			out = false,
			d = 0;
		// if (type == 'hand')
		// 	d = config.pointR-config.padding;

		var mx = config.width,
			my = config.height;
		if (config.proportional) {
			mx = 1;
			my = 1;
			d /= config.height;
		}

		if (pos.x-d < 0){
			ret.x = d;
			out = true;
		}
		if (pos.y-d < 0){
			ret.y = d;
			out = true;
		}
		if (pos.x+d > mx){
			ret.x = mx-d;
			out = true;
		}
		if (pos.y+d > my){
			ret.y = my-d;
			out = true;
		}
		if (out) return ret;
		return false;
	}

	var initBounds = this.outOfBounds(pos);
	if (initBounds)
		pos = initBounds;
	this.x = pos.x,
	this.y = pos.y;
	var origPos = {x:this.x,y:this.y};
	this.pos = function (pos) {
		if (pos === undefined)
			return {x:this.x,y:this.y};
		else {
			var p = this.outOfBounds(pos);
			if (p)
				pos = p;
			if (! this.fixed)
				this.x = pos.x;
			this.y = pos.y;
		}
	}
	this.hasMoved = function () {
		var d = {x:origPos.x-this.x,y:origPos.y-this.y};
		return Math.sqrt((d.x*d.x)+(d.y*d.y)) > 20;
	}
	this.draw = function () {
		var x = this.x,
			y = this.y;

		if (config.proportional) {
			x = this.x*config.width;
			y = this.y*config.height;
		}

		config.canvas.ctx.beginPath();
		config.canvas.ctx.arc(x,y,this.size,0,Math.PI*2);
		config.canvas.ctx.closePath();
		config.canvas.ctx.fillStyle = this.colour;
		config.canvas.ctx.fill();
	}
}

function Gradients(config,types,$parent) {
	var _this = this;



	var callbacks = {
			close:false,
			save:false,
			scroll:false,
			draw:false
		};
	this.callbacks = function (cbs) {
		for (var c in cbs)
			callbacks[c] = cbs[c];
	}

	var grads = {},
		selected = false;
	for (var t in types) {
		grads[t] = new Paths(config,types[t]);
	}
	for (var g in grads) {
		selected = g;
		break;
	}

	var $cont = $('<div>',{class:''});

	config.canvas = document.createElement('canvas');


	var sel = document.createElement('select');
	for (var g in grads) {
		var G = document.createElement('option');
		G.innerHTML = g;
		G.value = g;
		sel.appendChild(G);
	}
	sel.value = selected;
	sel.onchange = function () {
		selected = sel.value;
		config.canvas.redraw();
	}

	$cont.append(sel).append(config.canvas);

	config.canvas.ctx = config.canvas.getContext('2d');
	var selectedPoint = false;
	// config.canvas.ctx.moveTo();


	function prop (p) {
		if (config.proportional) {
			if (p=='x')
				return config.width;
			else if (p=='y')
				return config.height;
		} else return 1;
	}

	var
	dragOffset = {x:0,y:0},
	startDrag = function () {
		config.canvas.onmousemove = mousemove;
		config.canvas.onmouseup = mouseup
		config.canvas.onmouseout = mouseup;
	},
	mousedown = function (ev) {
		var paths = grads[selected];
		var point = paths.hitPoint(ev.offsetX,ev.offsetY);
		if (point) {
			dragOffset = {x:(ev.offsetX/prop('x'))-point.x,y:(ev.offsetY/prop('y'))-point.y};
			// get relative handle offsets
			var hands = paths.getHandles(point);
			dragOffset.hands = [];
			for (var h in hands) {
				var hand = hands[h];
				dragOffset.hands.push({
					point:hand,
					x:point.x-hand.x,
					y:point.y-hand.y
				});
			}
			selectedPoint = point;
			return startDrag();
		} else {
			var path = paths.hitPath(ev.offsetX,ev.offsetY);
			if (path)
				paths.select(path);
		}
	},
	mouseup = function () {
		config.canvas.removeEventListener('onmouseup',mouseup);
		config.canvas.onmouseup = undefined;
		config.canvas.removeEventListener('onmouseout',mouseup);
		config.canvas.onmouseout = undefined;
		config.canvas.removeEventListener('onmousemove',mousemove);
		config.canvas.onmousemove = undefined;
	},
	mousemove = function (ev) {
		selectedPoint.pos({x:(ev.offsetX/prop('x'))-dragOffset.x,y:(ev.offsetY/prop('y'))-dragOffset.y});
		for (var h in dragOffset.hands) {
			var hand = dragOffset.hands[h];
			hand.point.pos({
				x:selectedPoint.x-hand.x,
				y:selectedPoint.y-hand.y});
		}
		// move handles relative
		config.canvas.redraw();
	};
	config.canvas.onmousedown = mousedown;
	config.canvas.redraw = function () {
		config.canvas.ctx.clearRect(-config.padding,-config.padding,config.canvas.width+(config.padding*2),config.canvas.height+(config.padding*2));
		_this.draw();

	}

	config.canvas.ondblclick = function (ev) {
		var paths = grads[selected];
		var point = paths.hitPoint(ev.offsetX,ev.offsetY);
		if (point) {
			paths.removePoint(point);
		} else {
			var path = paths.hitPath(ev.offsetX,ev.offsetY);
			if (path) {
				paths.splitPath(path,(ev.offsetX-config.padding)/prop('x'),(ev.offsetY-config.padding)/prop('y'));
			} else paths.addPoint({x:(ev.offsetX-config.padding)/prop('x'),y:(ev.offsetY-config.padding)/prop('y')},'hand');
		}
	}


	this.draw = function () {
		// config.canvas.ctx.save();
		if (callbacks.draw)
			callbacks.draw(config);
		for (var g in grads) {
			if (g == selected)
				continue;
			grads[g].draw(false,'#ddd');
		}
		grads[selected].draw();
		// config.canvas.ctx.restore();
	}
	this.getGradients = function () {
		var ret = {};
		for (var g in grads)
			ret[g] = grads[g].getPaths();
		return ret;
	}
	this.getAtX = function (x) {
		var ret = {};
		for (var g in grads)
			ret[g] = grads[g].getAtX(x);
		return ret;
	}
	this.close = function () {
		if (callbacks.close)
			callbacks.close();
		$cont.remove();
	}
	this.resize = function (width,height) {
		if (width !== undefined)
			config.width = width;
		if (height !== undefined)
			config.height = height;
		config.canvas.height = config.height+(config.padding*2);
		config.canvas.width = config.width+(config.padding*2);
		config.canvas.ctx.translate(config.padding,config.padding);
		config.canvas.redraw();
	}
	this.resize();

	this.view = $cont;
	if ($parent !== undefined)
		$parent.append($cont);

}
