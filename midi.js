
var layouts = {
	querty:"1234567890qwertyuiopasdfghjklzxcvbnm"
}, layout = 'querty',
instruments = [
	'acoustic_grand_piano',
	// 'accordion',
	// 'acoustic_bass',
	// 'acoustic_guitar_nylon',
	// 'acoustic_guitar_steel',
	// 'banjo',
	// 'cello',
	// 'church_organ',
	// 'clarinet',
	// 'distortion_guitar',
	// 'flute',
	// 'trumpet',
	// 'violin',
	// 'trombone',
	// 'synth_drum',
],
all_instruments = [];

function notesFromLayout(lay) {
	var ll = layouts[lay].length,
		keyToNote = {},
		midc = Math.round(ll/2);
	keyToNote[midc] = "C4";
	var C = 67,
		UC = 67,
		DC = 67,
		UOC = 4,
		DOC = 4,
		LS = 65,
		LB = 71;
	for (var i=0;i<midc;i++) {
		UC += 1;
		DC -= 1;
		if (UC>LB)
			UC = LS;
		if (UC == C)
			UOC += 1;

		if (DC<LS)
			DC = LB;
		if (DC == C-1)
			DOC -= 1;

		if (midc+1+i < ll)
			keyToNote[midc+1+i] = String.fromCharCode(UC)+UOC;
		if (midc-(1+i) >= 0)
			keyToNote[midc-(1+i)] = String.fromCharCode(DC)+DOC;
	}
	return keyToNote;
}

function init () {
	$('body').empty();
	var keyToNote = notesFromLayout(layout),
		noteToKey = {},
		notesPressed = {},
		flatNotes = [],
		record = {
			startTime:0,
			active:'falsy',
			notes:[]
		},
		tracks = {},
		currentTrack = 0;

	var $bar = $('<header>',{id:'bar'}),
			$play = $('<button>',{id:'play',class:'fa fa-play'}),
			$instruments = $('<select>',{id:'instruments'}),
			$addInstrument = $('<button>',{id:'add-instrument',class:'fa fa-ellipsis-h',title:'Load More Instruments'}),
			$edit = $('<button>',{id:'edit',class:'fa fa-pencil'}),
		$tracks = $('<ol>',{id:'tracks'}),
		$notes = $('<ol>',{id:'notes'}),
		$fileActs = $('<div>',{id:'file-actions'}),
			$fileName = $('<input>',{id:'file-name',placeholder:"Tune Name"}),
			$fileSave = $('<button>',{id:'file-save',class:'fa fa-save',title:'Save Tune'}),
			$fileLoad = $('<button>',{id:'file-load',class:'fa fa-file-audio-o',title:'Load Tune'}),
			$fileNew = $('<button>',{id:'file-new',class:'fa fa-plus',title:'New Tune'});

	$('body')
		.append($bar
			.append($play)
			.append($instruments)
			.append($addInstrument)
			.append($edit)
			.append($fileActs
				.append($fileName)
				.append($fileSave)
				.append($fileLoad)
				.append($fileNew)
			))
		.append($tracks)
		.append($notes);



	for (var k in keyToNote) {
		var note = keyToNote[k];
		noteToKey[note] = k;
		notesPressed[note] = false;
		$notes.append($('<li>',{class:'note','data-note':note,'data-key':layouts[layout][k]}))
	}

	for (var k in keyToNote) {
		var note = keyToNote[k],
			bnote = note[0] + 'b' + note[1];
		if (bnote in MIDI.keyToNote) {
			noteToKey[bnote] = (k*1)-0.5;
			flatNotes.push(noteToKey[bnote]);
			notesPressed[bnote] = false;
			var letter = layouts[layout][k].toUpperCase();
			var map = {1:"!",2:'"',3:"\u00A3",4:"$",5:"%",6:"^",7:"&",8:"*",9:"(",0:")"}
			if (letter in map)
				letter = map[letter];
			$notes.find('.note[data-note='+note+']').append($('<div>',{class:'note flat','data-note':bnote,'data-key':letter}))
		}
	}


	for (var i in instruments) {
		var instrument = instruments[i];
		$instruments.append($('<option>',{value:instrument}).html(instrument.replace(/[\_]+/g," ")))
	}

	for (var i=0;i<1;i++)
		$tracks.append(newTrack(i));






	function newTrack(id) {
		tracks[id] = {notes:[],instrument:0,start:0,stop:0,loop:1,offset:0,muted:false,tempo:100,bar:4};
		return $('<li>',{class:'track','data-track':id})
		.append($('<div>',{class:'actions'})
			.append($('<button>',{class:'record fa fa-circle'}))
			.append($('<button>',{class:'play fa fa-play'})))
		.append($('<div>',{class:'notes'}))
		.append($('<div>',{class:'ticker'}))
		.append($('<div>',{class:'marker start'})
			.append($('<input>',{value:0,class:'marker-input offset', type:'number',min:0})))
		.append($('<div>',{class:'marker stop'})
			.append($('<input>',{value:1,class:'marker-input loop', type:'number',min:1,step:1})));
	}
	function activeTrack(id) {
		$tracks.find('.track.active').removeClass('active');
		$tracks.find('.track[data-track='+id+']').addClass('active');
		var instrument = tracks[id].instrument;
		$instruments.val(instruments[instrument]);
		setProgram(id,instrument);
		currentTrack = id;
	}
	function pulse(icon) {
		var $icon = $('<i>',{class:'pulse fa fa-'+icon});
		$icon.css({opacity:0,fontSize:100})
		$('body').append($icon);
		$icon.animate({opacity:1,fontSize:200},100,function () {
			$icon.animate({opacity:0,fontSize:150},100,function () {
				$icon.remove();
			})
		})
	}
	function displayNotes($track, notes) {
		var $notes = $track.find('.notes'),
			notesActive = {};
		for (var k in keyToNote)
			notesActive[keyToNote[k]] = false;
		$notes.empty();
		for (var n in notes) {
			var note = notes[n];
			if (note[0]) {
				notesActive[note[2]] = note[1];
			} else {
				if (notesActive[note[2]]) {
					$notes.append($('<div>',{class:'note'}).css({
						width:timePos(note[1] - notesActive[note[2]]),
						left:timePos(notesActive[note[2]]),
						bottom:(noteToKey[note[2]]/layouts[layout].length)*100+"%"
					}));
					notesActive[note[2]] = false;
				}
			}
		}
	}
	function timePos (a) {
		return Math.round(a/50);
	}
	function posTime (a) {
		return Math.round(a)*50;
	}
	function stopTrack(track) {
		var $track = $tracks.find('[data-track='+track+']'),
			$ticker = $track.find('.ticker');
		$ticker.stop(true,false).css('left',0);
		window.clearInterval(tracks[track].playing);
		tracks[track].playing = 'falsy';
		for(var n in tracks[track].notesActive) {
			var note = tracks[track].notesActive[n];
			if (note)
				note.stop();
		}
		if ($track.is('.waiting')) {
			$track.removeClass('waiting');
			window.clearTimeout(tracks[track].waiting);
		}
		if ($play.is('.active')){
			var toDo = $play.attr('data-tracks')*1,
				toDone = $play.attr('data-done')*1;
			toDone += 1;
			if (toDone == toDo) {
				$play.removeClass('active')
					.removeAttr('data-tracks')
					.removeAttr('data-done');
			} else $play.attr('data-done',toDone);
		}
	}
	function startTrack(track, loopsDone, offsetDone) {
		if (offsetDone === undefined)
			offsetDone = false;
		var Track = tracks[track];
		if (Track.offset && ! offsetDone) {
			var $track = $tracks.find('.track[data-track='+track+']');
			$track.addClass('waiting');
			Track.waiting = window.setTimeout(function () {
				$track.removeClass('waiting');
				startTrack(track, loopsDone, true);
			},Track.offset*1000)
			return;
		}
		if (loopsDone === undefined)
			loopsDone = 0;
		var notes = Track.notes,
			plays = $.extend([],notes,true).reverse(),
			notesActive = {},
			$track = $tracks.find('[data-track='+track+']'),
			$ticker = $track.find('.ticker'),
			pl = Track.start,
			startPos = timePos(pl),
			stopPos = Track.stop,
			stopLoops = Track.loop-1;
		Track.notesActive = notesActive;
		for (var k in keyToNote)
			notesActive[keyToNote[k]] = false;
		var pl = posTime(startPos),
			step = 500;
		$ticker.css('left',startPos);
		for (var p=plays.length-1;p>=0;p--) {
			if (plays[p][1] < pl)
				plays.splice(p,1);
			else break;
		}
		var playStep = function () {
			var endStep = pl+step;
			for (var p=plays.length-1;p>=0;p--) {
				var note = plays[p],
					d = note[1];
				if (d>pl&&d<=endStep) {
					if (note[0]) {
						notesActive[note[2]] = MIDI.noteOn(track,MIDI.keyToNote[note[2]],127,(note[1]-pl)/1000);
					} else {
						MIDI.noteOff(track,MIDI.keyToNote[note[2]],(note[1]-pl)/1000);
						notesActive[note[2]] = false;
					}
					plays.splice(p,1);
				}
				// ERR may be buggy if notes are out of order
				else break;
			}
			if (stopPos) {
				if (stopPos < endStep) {
					window.clearInterval(Track.playing);
					window.setTimeout(function () {
						var $track = $tracks.find('.track[data-track='+track+']'),
							repeat = loopsDone < stopLoops;
						if (repeat) {
							var trs = $play.attr('data-tracks')*1;
							$play.attr('data-tracks',trs+1);
						}
						stopTrack(track);
						if (repeat)
							startTrack(track, loopsDone+1, offsetDone);
						else $track.find('.play').removeClass('active');
					}, stopPos-pl);
				}
			}
			else if (plays.length == 0) {
				window.clearInterval(Track.playing);
				window.setTimeout(function () {
					stopTrack(track);
					$tracks.find('.track[data-track='+track+'] .play').removeClass('active');
				}, step)
			}
			$ticker.stop(true,false).animate({
				'left':timePos(endStep)
			},step,"linear");
			pl += step;
		};
		playStep();
		Track.playing = window.setInterval(playStep,step);
	}
	function getKey (ev) {
		var k;
		if (window.event)
			k = ev.keyCode;
		else k = ev.which;
		return k;
	}
	function getNote (ev) {
		var k = getKey(ev);

		// A - Z
		if (k>64&&k<91)
			k += 32;

		// a- z
		if ((k>96&&k<123)||(k>47&&k<58)) {
			var noteIndex = layouts[layout].indexOf(String.fromCharCode(k)),
				note = keyToNote[noteIndex];
			if (ev.shiftKey)
				note = note[0] + "b" + note[1];
			return note;
		}
		return 'falsy';
	}

	function setInstrument(track,instrument) {
		if (typeof instrument != 'string')
			instrument = instruments[instrument];
		MIDI.setInstrument(track, MIDI.GM.byName[instrument].number);
	}
	function setProgram(track, instrument) {
		if (typeof instrument != 'string')
			instrument = instruments[instrument];
		MIDI.programChange(track, MIDI.GM.byName[instrument].number);
	}
	function noteOn (note){
		MIDI.noteOn(currentTrack,MIDI.keyToNote[note],127,0);
		notesPressed[note] = true;
		if (record.active != "falsy") {
			record.notes.push([true, Date.now()-record.startTime, note]);

		}
		$notes.find('.note[data-note='+note+']').addClass('active');
	}
	function noteOff (note) {
		MIDI.noteOff(currentTrack,MIDI.keyToNote[note],0)
		notesPressed[note] = false;
		if (record.active != "falsy") {
			record.notes.push([false, Date.now()-record.startTime, note]);
		}
		$notes.find('.note[data-note='+note+']').removeClass('active');
	}
	function trackActions() {
		$tracks.find('.track').unbind('click dblclick').click(function (ev) {
			if($(ev.target).is('.play'))
				return;
			activeTrack($(this).attr('data-track'));
		}).dblclick(function (ev) {
			if ($(ev.target).is('.marker'))
				return;
			if ($(this).is('.recorded')) {
				$(this).toggleClass('muted');
				tracks[$(this).attr('data-track')].muted = $(this).hasClass('muted');
			}
		})
		$tracks.find('.track .marker').unbind('dblclick').dblclick(function() {
			var $marker = $(this);
			$marker.toggleClass('active');
			if ($marker.is('.active'))
				$marker.find('input').focus();
		})
		$tracks.find('.track .marker input').unbind('keypress change').keypress(function(ev) {
			var k = getKey(ev);
			if (k == 13) {
				$(this).blur()
				.closest('.marker').removeClass('active');
			}
		}).change(function () {
			var track = $(this).closest('.track').attr('data-track');

			if ($(this).is('.offset'))
				tracks[track].offset = $(this).val()*1;
			if ($(this).is('.loop'))
				tracks[track].loop = $(this).val()*1;
		})
		$tracks.find('.track .record').unbind('click').click(function () {
			var $butt = $(this),
				$track = $butt.closest('.track');
			if ($track.is('.active:not(.muted)')) {
				if ($butt.is('.active')) {
					$butt.removeClass('active')
					console.log('stop recording')
					if (record.notes.length) {
						tracks[record.active].notes = record.notes;
						displayNotes($track, record.notes);
						$track.addClass('recorded');
						record.notes = [];
					}
					if ($tracks.find('.track:not(.recorded)').length == 0 && $tracks.find('track').length<16) {
						$tracks.append(newTrack(Object.keys(tracks).length));
						trackActions();
					}

						$tracks.find('.track .play.active').each(function () {
							$(this).removeClass('active');
							stopTrack($(this).closest('.track').attr('data-track'));
						})
					record.active = 'falsy';
					pulse('stop');
				} else {
					$butt.addClass('active')
					console.log('recording')
					record.active = $track.attr('data-track');
					record.notes = [];
					// unclick active
					$tracks.find(".track.recorded .actions .play.active").click();
					// click other tracks on
					$tracks.find(".track.recorded:not([data-track="+record.active+"]):not(.muted) .actions .play").click();
					record.startTime = Date.now();
					pulse('circle');
				}
			}
		})
		$tracks.find('.track .play').unbind('click').click(function () {
			var $butt = $(this),
				$track = $butt.closest('.track'),
				track = $track.attr('data-track');
			if ($butt.is('.active')) {
				$butt.removeClass('active');
				stopTrack(track);
			} else {
				startTrack(track);
				$butt.addClass('active');
			}
		})
		$tracks.find('.marker:not(.ui-draggable)').draggable({
			axis:'x',
			containment:'parent',
			stop: function () {
				var $track = $(this).closest('.track'),
					track = $track.attr('data-track');
				$track.find('.time-marker').remove();
				if ($(this).is('.start'))
					tracks[track].start = posTime($(this).position().left);
				if ($(this).is('.stop'))
					tracks[track].stop = posTime($(this).position().left);
			},
			drag:function () {
				if ($(this).is('.stop')) {
					var $track = $(this).closest('.track'),
						end = $(this).position().left,
						start = $track.find('.marker.start').position().left,
						diff = posTime(end-start)/1000,
						$marker = $track.find('.time-marker');
					if (! $marker.length) {
						$marker = $('<div>',{class:'time-marker'});
						$track.append($marker);
					}
					$marker.html(diff).css({
						left:start,
						width:end-start
					});
				} else if ($(this).is('.start')) {
					var $track = $(this).closest('.track'),
						end = $(this).position().left,
						$marker = $track.find('.time-marker');
					if (! $marker.length) {
						$marker = $('<div>',{class:'time-marker'});
						$track.append($marker);
					}
					$marker.html(posTime(end)/1000).css({
						left:0,
						width:end
					});
				}
			}
		})
	}
	function loadFile(name) {
		var $popup = $('#popup'),
			file = JSON.parse(localStorage['midi-track-'+name]);
		for (var F in file) {
			var instrument = file[F][0];
			$popup.addClass('loading').html('Loading Instruments...')
			if (! instrument)
				file[F][0] = instrument = instruments[0];
			if (instruments.indexOf(instrument) == -1) {
				MIDI.loadResource({
					instrument:instrument,
					onsuccess:function () {
						instruments.push(instrument);
						$instruments.append($('<option>',{value:instrument}).html(instrument.replace(/[\_]+/g," ")));
						loadFile(name);
					}
				})
				return;
			}
		}
		if ($play.is('.active'))
			$play.click();
		$popup.remove();
		$fileName.val(name);
		tracks = {};
		$tracks.empty();
		for (var F in file) {
			var f = file[F];
			var $track = newTrack(F);
			$tracks.append($track);
			tracks[F].notes = f[1];
			var ind = instruments.indexOf(f[0]);
			tracks[F].instrument = ind;
			if (f.length > 2) {
				tracks[F].start = f[2];
				tracks[F].stop = f[3];
				tracks[F].offset = f[4];
				tracks[F].loop = f[5];
				tracks[F].muted = f[6];
				$track.find('.start').css('left', timePos(f[2]))
					.find('input').val(f[4]);
				$track.find('.stop').css('left', timePos(f[3]))
					.find('input').val(f[5]);
				if (f[6])
					$track.addClass('muted');
				if (f.length > 7) {
					tracks[F].tempo = f[7];
					tracks[F].bar = f[8];
				}
			}
			setProgram(F, f[0]);
			displayNotes($track, f[1]);
			$track.addClass('recorded')
		}
		if ($tracks.find('track').length<16)
			$tracks.append(newTrack(Object.keys(tracks).length));
		trackActions();
		activeTrack(0);
	}


	$edit.click(function () {

		var noteH = 20,
			barH= 18,
			factor = 20,
			notesW = 40,
			notesH = (noteH*Object.keys(keyToNote).length),
			t = $tracks.find('.track.active').attr('data-track'),
			track = tracks[t],
			bpm = track.tempo,
			bpb = track.bar;


		var $editor = $('<div>',{id:'editor','data-track':t}),
				$toolBar = $('<div>',{id:'edit-tools'}),
					$tempo = $('<input>',{id:'tempo',value:bpm,type:'number',min:1,max:999}),
					$beats = $('<input>',{id:'beats',value:bpb,type:'number',min:2,max:5}),
					$save = $('<button>',{id:'edit-save'}).html('save'),
					$cancel = $('<button>',{id:'edit-cancel'}).html('cancel'),
				$container = $('<main>',{id:'midi-editor'}),
					$head = $('<header>',{id:'midi-bars','data-bpb':bpb}),
					$notes = $('<header>',{id:'midi-notes'}),
					$grid = $('<div>',{id:'midi-grid'}).css({height:notesH});
		$('body').append($editor
				.append($toolBar
					.append($tempo)
					.append($beats)
					.append($save)
					.append($cancel))
					.append($container
						.append($notes)
						.append($head)
						.append($grid)))
		function displayGrid(bpm,bpb) {
			var barsN = 4;

			if (track.notes.length)
				barsN = Math.ceil(bpm*(track.notes[track.notes.length-1][1]/1000)/(60*bpb));

			// no table, just do bars & notes then pos manually

			var noteW = 60*1000/(bpm*factor)

			$head.empty().attr('data-bpb', bpb);
			$notes.empty();
			$grid.empty().css('width', barsN*noteW*bpb);
			for (var b=0;b<barsN;b++)
				$head.append($('<div>',{class:'bar'}).html(b+1).css({width:noteW*bpb,height:notesH+barH}));
			// x = b/m
			for (var k in keyToNote) {
				var note = keyToNote[k];
				$notes.prepend($('<div>',{class:'note'}).html(note).css({width:(noteW*bpb*barsN)+notesW}))
			}

			var notesActive = {};
			for (var k in keyToNote)
				notesActive[keyToNote[k]] = false;
			for (var n in track.notes) {
				var note = track.notes[n];
				if (note[0]) {
					notesActive[note[2]] = note[1];
				} else {
					if (notesActive[note[2]]) {
						// $notes.append($('<div>',{class:'note'}).css();
						$grid.append($('<div>',{class:'grid-note'}).css({
							width:(note[1] - notesActive[note[2]])/factor,
							left:notesActive[note[2]]/factor,
							bottom:noteToKey[note[2]]*noteH
						}))
						notesActive[note[2]] = false;
					}
				}
			}

			function gridNoteActions($gridNotes) {
				$gridNotes.resizable({
					handles:'e,w',
					start:function (ev,ui) {
						var $note = $(this),
							d = $note.data('ui-resizable').axis;
						// $note.attr('data-right', pl+pw);
						if (! $note.is('.active')) {
							$grid.find('.grid-note.active').removeClass('active');

							$note.addClass('active');
						}
						var min = 'falsy',max='falsy';
						$grid.find('.grid-note.active').each(function () {
							var $note = $(this),
								l = $note.position().left,
								r = $note.position().left+$note.width();
							$note.attr('data-left',l)
							$note.attr('data-right',r)
							if (min == 'falsy' || l < min)
								min = l;
							if (max == 'falsy' || r > max)
								max = r;
						}).attr({'data-min':min,'data-max':max});
					},
					resize:function (ev,ui) {
						var $note = $(this),
							d = $note.data('ui-resizable').axis;
						if (d == "w") {
							var pr = $note.attr('data-right')*1,
								nl = Math.round((ui.position.left*2/noteW))*noteW/2;
							if ($note.is('.active')) {
								var min=$note.attr('data-min')*1,
									max=$note.attr('data-max')*1,
									fac = (max-nl)/(max-($note.attr('data-left')*1));
								$grid.find('.grid-note.active').each(function () {
									var $note = $(this),
										l = $note.attr('data-left')*1,
										r = $note.attr('data-right')*1,
										nl = max-((max-l)*fac),
										nr = max-((max-r)*fac);
									$note.css({
										left:nl,
										width:nr-nl
									});
								});
							} else {
								$note.css({
									left:nl,
									width:pr-nl
								});
							}
						} else {
							var nw = (Math.round((ui.position.left+ui.size.width)*2/noteW)*noteW/2);
							if ($note.is('.active')) {
								var min=$note.attr('data-min')*1,
									max=$note.attr('data-max')*1,
									fac = (nw-min)/(($note.attr('data-right')*1)-min);
								$grid.find('.grid-note.active').each(function () {
									var $note = $(this),
										l = $note.attr('data-left')*1,
										r = $note.attr('data-right')*1,
										nl = ((l-min)*fac)+min,
										nr = ((r-min)*fac)+min;
									$note.css({
										left:nl,
										width:nr-nl
									});
								});
							} else {
								$note.css({
									width:nw-ui.position.left
								});
							}
						}
					},
					stop:function () {
						$grid.find('.grid-note').removeAttr('data-right data-left data-min data-max');
						return;
					},
					containment:"parent"
				})
				.draggable({
					start:function (ev,ui) {
						var $note = $(this);
						if ($note.is('.active')) {
							$grid.find('.grid-note.active').each(function () {
								var $note = $(this);
								$note.attr('data-left',$note.position().left)
								$note.attr('data-top',$note.position().top)
							});
						} else {
							$grid.find('.grid-note.active').removeClass('active');
							$note.addClass('active');
						}
					},
					drag:function (ev,ui) {
						var $note = $(this),
							pl = ui.position.left,
							pt = ui.position.top,
							nl = Math.round((pl*2/noteW))*noteW/2,
							nt = Math.round((pt*2/noteH))/2;
						if (nt%1 && flatNotes.indexOf(Object.keys(keyToNote).length-(1+nt)) == -1)
							ui.position.top = $note.position().top;
						else ui.position.top = nt*noteH;
						ui.position.left = nl;

						var tdelt = {
							top:ui.position.top-($note.attr('data-top')*1),
							left:ui.position.left-($note.attr('data-left')*1),
						}
						$grid.find('.grid-note.active').each(function () {
							var $note = $(this),
								opos = {
									top:$note.attr('data-top')*1,
									left:$note.attr('data-left')*1,
								};
							$note.css({
								top:opos.top+tdelt.top,
								left:opos.left+tdelt.left,
							})
						})
					},
					stop:function (ev,ui) {
						var $note = $(this),
							pl = ui.position.left,
							pt = ui.position.top,
							nl = Math.round((pl*2/noteW))*noteW/2,
							nt = Math.round((pt*2/noteH))*noteH/2;
						ui.position.left = nl;
						ui.position.top = nt;
					},
					containment:"parent"
				})
				.dblclick(function () {
					$(this).remove();
				})
			}
			gridNoteActions($grid.find('.grid-note'));

			$grid
			.dblclick(function (ev) {
				if (this != ev.target)
					return;
				var pl = ev.offsetX,
					pt = ev.offsetY,
					nl = Math.floor(pl/noteW),
					nt = Math.floor(pt/noteH);
				if (nt%1 && flatNotes.indexOf(nt) == -1)
					nt = Math.ceil(nt);
				var $newNote = $('<div>',{class:'grid-note'})
					.css({
						left:nl*noteW,
						top:nt*noteH,
						width:noteW,
						height:noteH
					});
				$grid.append($newNote);
				gridNoteActions($newNote);
			})
			.mousedown(function (ev) {
				if ($(ev.target).is('.grid-note,.ui-resizable-handle'))
					return;
				var pos = {left:ev.pageX,top:ev.pageY},
					$select = $('<div>',{id:'grid-select'}).css(pos);
				$grid.append($select);
				$grid.mousemove(function (ev) {
					var pos1 = $.extend({},pos),
						pos2 = {left:ev.pageX,top:ev.pageY};
					if (pos1.left > pos2.left) {
						var p = pos1.left;
						pos1.left = pos2.left;
						pos2.left = p;
					}
					if (pos1.top > pos2.top) {
						var p = pos1.top;
						pos1.top = pos2.top;
						pos2.top = p;
					}
					$select.css(pos1).css({width:pos2.left-pos1.left,height:pos2.top-pos1.top})

					var rpos = {
						left:$grid.offset().left,
						top:$grid.offset().top,
					}

					$grid.find('.grid-note').each(function () {
						var $note = $(this);
						if ($note.position().left+rpos.left > pos1.left &&
							$note.position().left+rpos.left+$note.width() < pos2.left &&
							$note.position().top+rpos.top > pos1.top &&
							$note.position().top+rpos.top+$note.height() < pos2.top)
							$note.addClass('active');
						else $note.removeClass('active');
					})
				})
				$grid.mouseup(function () {
					$select.remove();
					$grid.unbind('mousemove mouseup')
				})
			})
		}
		displayGrid(bpm,bpb)

		$cancel.click(function () {
			$editor.remove();
		})
		$save.click(function () {
			var notes = [];
			var noteW = 60*1000/(bpm*factor)
			$grid.find('.grid-note').each(function () {
				var $note = $(this),
					pl = $note.position().left,
					pt = $note.position().top,
					nl = Math.round(pl*factor),
					sl = Math.round((pl+$note.outerWidth())*factor),
					nt = Math.round((pt*2/noteH))/2,
					k = Object.keys(keyToNote).length-(nt+1),
					nk;
				if (k in keyToNote)
					nk = keyToNote[k];
				else {
					var pk = keyToNote[Math.ceil(k)];
					nk = pk[0]+"b"+pk[1];
				}
				if (nl < 1)
					nl = 1;
				if (sl < 1)
					sl = 1;

				notes.push([true,nl,nk]);
				notes.push([false,sl,nk]);
			})
			notes.sort(function (a,b) {
				return a[1]-b[1];
			})
			track.notes = notes;
			track.tempo = bpm;
			track.bars = bpb;
			var t = $editor.attr('data-track');
			displayNotes($tracks.find('.track[data-track='+t+']'), notes);
			$editor.remove();
		})
		$tempo.change(function () {
			bpm = $(this).val()*1;
			displayGrid(bpm,bpb)
		})
		$beats.change(function () {
			bpb = $(this).val()*1;
			displayGrid(bpm,bpb)
		})
		$container.scroll(function () {
			$head.css('left', -$(this).scrollLeft());
			$notes.css('top', 30-$(this).scrollTop());
		})

	})
	$addInstrument.click(function () {
		var $popup = $('<div>',{id:'popup'});
		$('body').append($popup);
		$popup.dblclick(function() {$(this).remove();});
		for (var i in all_instruments) {
			var instrument = all_instruments[i];
			if (instruments.indexOf(instrument) == -1) {
				$popup.append($('<button>',{class:'instrument','data-instrument':instrument}).html(instrument.replace(/[\_]+/g,' ')));
			}
		}
		$popup.find('.instrument').click(function () {
			var instrument = $(this).attr('data-instrument');
			$popup.addClass('loading').html('Loading Instrument...')
			MIDI.loadResource({
				instrument:instrument,
				onsuccess:function () {
					instruments.push(instrument);
					$instruments.append($('<option>',{value:instrument}).html(instrument.replace(/[\_]+/g," ")));
					$popup.remove();
				}
			})
		})
	})
	$fileNew.click(function () {
		if ($play.is('.active'))
			$play.click();
		tracks = {};
		$tracks.empty();
		for (var i=0;i<1;i++)
			$tracks.append(newTrack(i));
		$fileName.val('');
		activeTrack(0);
		trackActions();
		pulse('plus');
	})
	$fileSave.click(function () {
		var name = $fileName.val();
		if (name) {
			var store = [];
			for (var t in tracks) {
				var track = tracks[t];
				if (track.notes.length)
					store.push([instruments[track.instrument], track.notes, track.start*1,track.stop*1,track.offset*1,track.loop*1,track.muted,track.tempo,track.bar]);
			}
			if (store.length) {
				localStorage['midi-track-'+name] = JSON.stringify(store);
				pulse('save');
			}
		} else $fileName.focus();
	})
	$fileLoad.click(function () {
		var $popup = $('<div>',{id:'popup'});
		$('body').append($popup);
		$popup.dblclick(function() {$(this).remove();});
		var test = 'midi-track-',
			tl = test.length;
		for (var l in localStorage){
			if (l.slice(0,tl)==test) {
				var name = l.slice(tl);
				$popup.append($('<button>',{class:'file','data-file':name}).html(name));
			}
		}
		$popup.find('.file').click(function () {
			loadFile($(this).attr('data-file'));
		});
	})
	trackActions();
	$instruments.change(function () {
		var $track = $tracks.find('.track.active'),
			track  = $track.attr('data-track'),
			ind = instruments.indexOf($(this).val());
		tracks[track].instrument = ind;
		setInstrument(track, $(this).val());
	})
	$('body').keydown(function (ev) {
		if ($(ev.target).is('input,select'))
			return;
		var note = getNote(ev);

		if (note == 'falsy' && getKey(ev) == 16) {
			var N;
			for (var n in notesPressed) {
				if (n[1] != 'b' && notesPressed[n])
					noteOff(n);
			}

		}
		if (note != 'falsy') {
			if (! notesPressed[note]) {
				noteOn(note);
			}
		}
	});
	$('body').keyup(function (ev) {
		if ($(ev.target).is('input,select'))
			return;
		var note = getNote(ev);

		if (note == 'falsy' && getKey(ev) == 16){
			for (var n in notesPressed) {
				if (n[1] == 'b' && notesPressed[n]) {
					note = n;
					break;
				}
			}
		}
		if (note != 'falsy') {
			noteOff(note);
		}
	});
	$play.click(function () {
		if($(this).is('.active')) {
			$(this).removeClass('active')
				.removeAttr('data-tracks')
				.removeAttr('data-done');
			$tracks.find(".track.recorded .actions .play.active").click();

		} else {
			$tracks.find(".track.recorded .actions .play.active").click();

			var $cls = $tracks.find(".track.recorded:not(.muted) .actions .play");
			if ($cls.length) {
				$(this).attr({
					'data-tracks':$cls.length,
					'data-done':0
				});
				$(this).addClass('active');
				$cls.click();
			}
		}
	})


	setInstrument(0,0);
	activeTrack(0);

}



$(function () {
	MIDI.loadPlugin({
		instruments:instruments,
		onsuccess:function () {
			$.get('soundfont/names.json',function (data) {
				all_instruments = data;
				init();
			})
		}
	})
})
