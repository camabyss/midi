# Base Project
Clone the MIDI.js project into the main directory, it should automatically go into a new folder called "MIDI.js"

https://github.com/mudcube/MIDI.js/

# Instrument Files
Download the sound font package, it's best to download as a zip instead of cloning because it's huge
https://github.com/gleitz/midi-js-soundfonts
Inside this project you should find two folders: "FluidR3_GM" and "MusyngKite", these are two different sound fonts. MusyngKite is much higher quality but the files take longer to load.

Create a link from one of these folders to the base of this project, name it "soundfont" and it will be automatically picked up.
